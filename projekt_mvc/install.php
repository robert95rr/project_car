<!doctype html>
<html lang="pl">
    <head>
        <meta charset="utf-8">
        <title>Instalacja</title>
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <?php
        require 'vendor/autoload.php';

        use Config\Database\DBConfig as DB;
        use Config\Database\DBConnection as DBConnection;

        DBConnection::setDBConnection(DB::$user,DB::$password, 
                                      DB::$hostname, DB::$databaseType, DB::$port);	
        try {
            $pdo =  DBConnection::getHandle();
        }catch(\PDOException $e){
            echo \Config\Database\DBErrorName::$connection;
            exit(1);
        }   

		/*
        usunięcie starych danych   
        */
        $query = 'DROP TABLE IF EXISTS  `'.DB::$tableCar.'`';
        try
        {
            $pdo->exec($query);
        }
        catch(\PDOException $e)
        {
            echo $e;
            echo \Config\Database\DBErrorName::$delete_table.DB::$tableCar;
        }         	
        
        $query = 'DROP TABLE IF EXISTS `'.DB::$tableOwner.'`';
        try
        {
            $pdo->exec($query);
        }
        catch(\PDOException $e)
        {
            echo $e;
            echo \Config\Database\DBErrorName::$delete_table.DB::$tableOwner;
        }      

		$query = 'DROP TABLE IF  EXISTS `'.DB::$tableBodyType.'`';
        try
        {
            $pdo->exec($query);
        }
        catch(\PDOException $e)
        {
            echo $e;
            echo \Config\Database\DBErrorName::$delete_table.DB::$tableBodyType;
        }         		   		
		
			   		
		
		$query = 'DROP TABLE IF  EXISTS `'.DB::$tableAccount.'`';
        try
        {
            $pdo->exec($query);
        }
        catch(\PDOException $e)
        {
            echo $e;
            echo \Config\Database\DBErrorName::$delete_table.DB::$tableAccount;
        }         		   		
		
        /*
        tworzenie tabeli wlasciciela
        */
        $query = 'CREATE TABLE IF NOT EXISTS `'.DB::$tableOwner.'` (
		`'.DB\Owner::$idOwner.'` INT NOT NULL AUTO_INCREMENT,
		`'.DB\Owner::$name.'` VARCHAR(30) NOT NULL,
        `'.DB\Owner::$surname.'` VARCHAR(30) NOT NULL,
        `'.DB\Owner::$phone.'` VARCHAR(16) NOT NULL,
		PRIMARY KEY ('.DB\Owner::$idOwner.')) ENGINE=InnoDB;';    
        try
        {
            $pdo->exec($query);
        }
        catch(\PDOException $e)
        {
            echo \Config\Database\DBErrorName::$create_table.DB::$tableOwner;
        }	
		
		/*
        tworzenie tabeli typ nadwozia
		*/
		$query = 'CREATE TABLE IF NOT EXISTS `'.DB::$tableBodyType.'` (
		`'.DB\BodyType::$idBodyType.'` INT NOT NULL AUTO_INCREMENT,
		`'.DB\BodyType::$name.'` VARCHAR(20) NOT NULL,
		PRIMARY KEY ('.DB\BodyType::$idBodyType.')) ENGINE=InnoDB;';    
		try
		{
			$pdo->exec($query);
		}
		catch(\PDOException $e)
		{
			echo \Config\Database\DBErrorName::$create_table.DB::$tableBodyType;
		}	
		
		/*
        tworzenie tabeli samochodu
        */
        $query = 'CREATE TABLE IF NOT EXISTS `'.DB::$tableCar.'` (
		`'.DB\Car::$idCar.'` INT NOT NULL AUTO_INCREMENT,
		`'.DB\Car::$brand.'` VARCHAR(25) NOT NULL,
        `'.DB\Car::$model.'` VARCHAR(20) NOT NULL,
        `'.DB\Car::$yearProduction.'` INT NOT NULL,
		`'.DB\Car::$engineSize.'` VARCHAR(4) NOT NULL,
		`'.DB\Car::$enginePower.'` INT NOT NULL,
		`'.DB\Car::$registrationNumber.'` VARCHAR(10) NOT NULL,
		`'.DB\Car::$vinNumber.'` VARCHAR(20) NOT NULL,
		`'.DB\Car::$fuel.'` VARCHAR(10) NOT NULL,
		`'.DB\Car::$colour.'` VARCHAR(20) NOT NULL,
		`'.DB\Car::$idOwner.'` INT NOT NULL,
		`'.DB\Car::$idBodyType.'` INT NOT NULL,
		PRIMARY KEY ('.DB\Car::$idCar.'),
		FOREIGN KEY ('.DB\Car::$idOwner.') REFERENCES '.DB::$tableOwner.'('.DB\Owner::$idOwner.') ON DELETE CASCADE,
		FOREIGN KEY ('.DB\Car::$idBodyType.') REFERENCES '.DB::$tableBodyType.'('.DB\BodyType::$idBodyType.') ON DELETE CASCADE
		) ENGINE=InnoDB;';    
        try
        {
            $pdo->exec($query);
        }
        catch(\PDOException $e)
        {
            echo \Config\Database\DBErrorName::$create_table.DB::$tableCar;
        }	
		
		/*
        tworzenie tabeli konto
		*/
		$query = 'CREATE TABLE IF NOT EXISTS '.DB::$tableAccount.' (
		'.DB\Account::$idAccount.' INT NOT NULL AUTO_INCREMENT,
		'.DB\Account::$login.' VARCHAR(15) NOT NULL UNIQUE,
		'.DB\Account::$haslo.' VARCHAR(32) NOT NULL,
		PRIMARY KEY ('.DB\Account::$idAccount.')) ENGINE=InnoDB;';    
		try
		{
			$pdo->exec($query);
		}
		catch(\PDOException $e)
		{
			echo \Config\Database\DBErrorName::$create_table.DB::$tableAccount;
		}	
		
		/*Stare!!!!
		$query = 'CREATE TABLE IF NOT EXISTS `'.DB::$tableAccount.'` (
		`'.DB\Account::$idAccount.'` INT NOT NULL AUTO_INCREMENT,
		`'.DB\Account::$login.'` VARCHAR(15) NOT NULL,
		`'.DB\Account::$haslo.'` VARCHAR(20) NOT NULL,
		PRIMARY KEY ('.DB\Account::$idAccount.')) ENGINE=InnoDB;';    
		try
		{
			$pdo->exec($query);
		}
		catch(\PDOException $e)
		{
			echo \Config\Database\DBErrorName::$create_table.DB::$tableAccount;
		}	
        */
        
        /*
        wypełnienie tabel danymi
        */  

        $owners = array();	
        $owners[] = array(
						    'name' => 'Tomasz',
						    'surname' => 'Nowak',
						    'phone' => '790-234-045');
        $owners[] = array(
						    'name' => 'Jacek',
						    'surname' => 'Dokan',
						    'phone' => '689-045-234');
        $owners[] = array(
						    'name' => 'Natalia',
						    'surname' => 'Rozen',
						    'phone' => '598-034-223');
        $owners[] = array(
						    'name' => 'Klara',
						    'surname' => 'Piotrowska',
						    'phone' => '956-340-556');
        $owners[] = array(
						    'name' => 'Piotr',
						    'surname' => 'Kowalski',
						    'phone' => '390-667-083');
        

        try
        {
           $stmt = $pdo -> prepare('INSERT INTO `'.DB::$tableOwner.'` (`'.DB\Owner::$name.'`, `'.DB\Owner::$surname.'`, `'.DB\Owner::$phone.'`) VALUES(:name, :surname, :phone)');	
		foreach($owners as $owner)
		{
			//strval($float), nie ma typu PDO::PARAM_FLOAT
			$stmt -> bindValue(':name', $owner['name'], PDO::PARAM_STR);
			$stmt -> bindValue(':surname', $owner['surname'], PDO::PARAM_STR);
			$stmt -> bindValue(':phone', $owner['phone'], PDO::PARAM_STR);
			$stmt -> execute(); 
		}
	}
	catch(\PDOException $e)
	{
		echo \Config\Database\DBErrorName::$noadd;
	} 

		$bodytypes = array();
		$bodytypes[] = 'Hatchback';
		$bodytypes[] = 'Kombi';
		$bodytypes[] = 'Sedan';
		$bodytypes[] = 'Coupe';
		$bodytypes[] = 'Minivan';
		$bodytypes[] = 'Limuzyna';
		$bodytypes[] = 'Van';
		$bodytypes[] = 'SUV';
		
		try
		{
			$stmt = $pdo -> prepare('INSERT INTO `'.DB::$tableBodyType.'` (`'.DB\BodyType::$name.'`) VALUES(:name)');	
		foreach($bodytypes as $bodytype)
		{
			$stmt -> bindValue(':name', $bodytype, PDO::PARAM_STR);
			$stmt -> execute(); 
		}
	}
	catch(\PDOException $e)
	{
		echo \Config\Database\DBErrorName::$noadd;
	}	
		
		
        $cars = array();	
        $cars[] = array(
						    'brand' => 'Volvo',
						    'model' => 'S90',
						    'yearProduction' => '2017',
							'engineSize' => '1969',
							'enginePower' => '235',
							'registrationNumber' => 'PKA_3OP9',
							'vinNumber' => 'B70FA44XU11593006',
							'fuel' => 'Diesel',
							'colour' => 'Szary',
							'idOwner' => '1',
							'idBodyType' => '3');
        $cars[] = array(
						    'brand' => 'Opel',
						    'model' => 'Astra',
						    'yearProduction' => '2010',
							'engineSize' => '2000',
							'enginePower' => '160',
							'registrationNumber' => 'WE_575CW',
							'vinNumber' => 'H4TK67S2052246801',
							'fuel' => 'Diesel',
							'colour' => 'Szary',
							'idOwner' => '2',
							'idBodyType' => '1');
		$cars[] = array(
						    'brand' => 'Mazda',
						    'model' => 'CX-5',
						    'yearProduction' => '2017',
							'engineSize' => '1998',
							'enginePower' => '160',
							'registrationNumber' => 'ZDR_4JC7',
							'vinNumber' => 'G6TR298ODF3009178',
							'fuel' => 'Benzyna',
							'colour' => 'Bialy',
							'idOwner' => '4',
							'idBodyType' => '8');
		$cars[] = array(
						    'brand' => 'Citroen',
						    'model' => 'C5',
						    'yearProduction' => '2012',
							'engineSize' => '1600',
							'enginePower' => '156',
							'registrationNumber' => 'DW_B25L',
							'vinNumber' => 'F2GV760SZU6609137',
							'fuel' => 'Benzyna',
							'colour' => 'Czarny',
							'idOwner' => '5',
							'idBodyType' => '2');
		$cars[] = array(
						    'brand' => 'Audi',
						    'model' => 'S5',
						    'yearProduction' => '2009',
							'engineSize' => '4163',
							'enginePower' => '354',
							'registrationNumber' => 'WU_88KH2',
							'vinNumber' => 'G61TE9KM5B0048369',
							'fuel' => 'Benzyna',
							'colour' => 'Czerwony',
							'idOwner' => '3',
							'idBodyType' => '4');
        

        try
        {
           $stmt = $pdo -> prepare('INSERT INTO `'.DB::$tableCar.'` (`'.DB\Car::$brand.'`, `'.DB\Car::$model.'`, `'.DB\Car::$yearProduction.'`, `'.DB\Car::$engineSize.'`, `'.DB\Car::$enginePower.'`, `'.DB\Car::$registrationNumber.'`, `'.DB\Car::$vinNumber.'`, `'.DB\Car::$fuel.'`, `'.DB\Car::$colour.'`, `'.DB\Car::$idOwner.'`, `'.DB\Car::$idBodyType.'`) VALUES(:brand, :model, :yearProduction, :engineSize, :enginePower, :registrationNumber, :vinNumber, :fuel, :colour, :idOwner, :idBodyType)');	
		foreach($cars as $car)
		{
			//strval($float), nie ma typu PDO::PARAM_FLOAT
			$stmt -> bindValue(':brand', $car['brand'], PDO::PARAM_STR);
			$stmt -> bindValue(':model', $car['model'], PDO::PARAM_STR);
			$stmt -> bindValue(':yearProduction', $car['yearProduction'], PDO::PARAM_STR);
			$stmt -> bindValue(':engineSize', $car['engineSize'], PDO::PARAM_STR);
			$stmt -> bindValue(':enginePower', $car['enginePower'], PDO::PARAM_STR);
			$stmt -> bindValue(':registrationNumber', $car['registrationNumber'], PDO::PARAM_STR);
			$stmt -> bindValue(':vinNumber', $car['vinNumber'], PDO::PARAM_STR);
			$stmt -> bindValue(':fuel', $car['fuel'], PDO::PARAM_STR);
			$stmt -> bindValue(':colour', $car['colour'], PDO::PARAM_STR);
			$stmt -> bindValue(':idOwner', $car['idOwner'], PDO::PARAM_STR);
			$stmt -> bindValue(':idBodyType', $car['idBodyType'], PDO::PARAM_STR);
			$stmt -> execute(); 
		}
	}
	catch(\PDOException $e)
	{
		echo \Config\Database\DBErrorName::$noadd;
	} 
	
		$accounts = array();	
        $accounts[] = array(
						    'login' => 'robert2017',
						    'haslo' => '480Hkw$021z');
       
        
        try
        {
           $stmt = $pdo -> prepare('INSERT INTO `'.DB::$tableAccount.'` (`'.DB\Account::$login.'`, `'.DB\Account::$haslo.'`) VALUES(:login, :haslo)');	
		foreach($accounts as $account)
		{
			//strval($float), nie ma typu PDO::PARAM_FLOAT
			$stmt -> bindValue(':login', $account['login'], PDO::PARAM_STR);
			$stmt -> bindValue(':haslo', md5($account['haslo']), PDO::PARAM_STR);
			$stmt -> execute(); 
		}
	}
	catch(\PDOException $e)
	{
		echo \Config\Database\DBErrorName::$noadd;
	} 
	
    echo "<b>Instalacja aplikacji zakończona!</b>"
        ?>
    </body>
</html>