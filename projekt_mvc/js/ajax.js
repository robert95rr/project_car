$(document).ready(function() {
    $('#addBtn').on('click', function() {
        $('.modal-body').find('.alert').remove();
        if($('#addInput').val() == ''){
            $('.modal-body').append('<div class="alert alert-danger">Wpisz nazwę!</div>');
        } else {
            var serializeData = $('#addForm').serialize();
            $.ajax({
              method: "post",
              url: "index.php?controller=bodytype&action=add",
              data:  serializeData,
              success: function(data) {
                console.log(data);
                var test = JSON.parse(data);
                console.log(test);
                var newRecord = '<tr id="'+ test[test.length - 1][0] +'">' +
                                  '<td>'+ test[test.length - 1][0] +'</td>' +
                                  '<td>'+ test[test.length - 1][1] +'</td>' +
                            			'<td><a href="http://localhost/Projekt_Car/projekt_mvc/car/intype/'+ test[test.length - 1][0] +'" data-toggle="tooltip" class="tip-right" data-original-title="Wyświetl wszystkie auta tego typu nadwozia"><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span>&nbsp;&nbsp;zobacz</a></td>' +
                                  '<td>' +
                                    '<a type="button" class="btn btn-warning btn-xs" href="http://localhost/Projekt_Car/projekt_mvc/bodytype/editform/'+ test[test.length - 1][0] +'">edytuj</a>&nbsp;&nbsp;' +
                                    '<a type="button" class="btn btn-danger btn-xs" href="http://localhost/Projekt_Car/projekt_mvc/bodytype/delete/'+ test[test.length - 1][0] +'">usuń</a>' +
                            			'</td>' +
          			                 '</tr>';
                $('#addInput').val("");
                var id = test[test.length - 2][0];
                $("tr#" + id).after(newRecord);
                setTimeout(function(){
                  $('.modal-body').append('<div class="alert alert-success">Rekord dodano do bazy!</div>');
                }, 1000);

              },
              error: function(data){
                console.log('Error:', data);
              }
            });
        }
    });
});
