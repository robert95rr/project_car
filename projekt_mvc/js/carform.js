$(document).ready(function() {
	//tutaj wstawiamy kod JQuery, który zostanie uruchomiony
	//jak tylko dokument będzie gotowy do manipulowania jego elementami
	/**
		Własne metody do walidacji
	**/

	$.validator.addMethod('registrationNumber', function (value, element) {
		return /^[A-Z]{2,3}_[A-Z0-9]{4,5}$/.test(value);
		}, 'Numer rejestracyjny musi zawierać tylko litery i cyfry!');	

	$.validator.addMethod('vinNumber', function (value, element) {
	return /^[0-9ABCDEFGHJKLMNPRSTUWVXYZ]{17}$/.test(value);
		}, 'Numer VIN musi zawierać tylko cyfry i litery z wyłączeniem I, O oraz Q!');			
	
    $('#carform').validate({
		//reguły dla pola formularza
        rules: {
			//atrybut: {reguły}
			brand: {
				//reguły - kolejność ma znaczenia
                required: true,
				minlength: 3,
				maxlength: 25
            },
			model: {
                required: true,
				minlength: 1,
				maxlength: 25
			},
			yearProduction: {
                required: true,
				minlength: 4,
				maxlength: 4
			},
			engineSize: {
                required: true,
				minlength: 3,
				maxlength: 4
			},
			enginePower: {
                required: true,
				minlength: 2,
				maxlength: 3
			},
			registrationNumber: {
                required: true,
				registrationNumber: true,
				minlength: 7,
				maxlength: 9
			},
			vinNumber: {
                required: true,
				minlength: 17,
                maxlength: 17,
                vinNumber: true
				
			},
			fuel: {
                required: true,
				minlength: 3,
				maxlength: 8
			},
			colour: {
                required: true,
				minlength: 4,
				maxlength: 20
			},
			bodytype: {
                required: true
			},
			owner: {
                required: true
			}
        },
		//komunikaty dla pola formularza
		messages: {
			brand: {
				required: 'Pole wymagane',
				minlength: jQuery.validator.format("Pole musi zawierać minimum {0} znaki!"),
				maxlength: jQuery.validator.format("Pole może zawierać maksimum {0} znaki!")
			},
			model: {
				required: 'Pole wymagane',
				minlength: jQuery.validator.format("Pole musi zawierać minimum {0} znaki!"),
				maxlength: jQuery.validator.format("Pole może zawierać maksimum {0} znaki!")
			},
			yearProduction: {
				required: 'Pole wymagane',
				minlength: jQuery.validator.format("Pole musi zawierać minimum {0} znaki!"),
				maxlength: jQuery.validator.format("Pole może zawierać maksimum {0} znaki!")
			},
			engineSize: {
				required: 'Pole wymagane',
				minlength: jQuery.validator.format("Pole musi zawierać minimum {0} znaki!"),
				maxlength: jQuery.validator.format("Pole może zawierać maksimum {0} znaki!")
			},
			enginePower: {
				required: 'Pole wymagane',
				minlength: jQuery.validator.format("Pole musi zawierać minimum {0} znaki!"),
				maxlength: jQuery.validator.format("Pole może zawierać maksimum {0} znaki!")
			},
			registrationNumber: {
				required: 'Pole wymagane',
				minlength: jQuery.validator.format("Pole musi zawierać minimum {0} znaki!"),
				maxlength: jQuery.validator.format("Pole może zawierać maksimum {0} znaki!")
			},
			vinNumber: {
				required: 'Pole wymagane',
				minlength: jQuery.validator.format("Pole musi zawierać minimum {0} znaki!"),
				maxlength: jQuery.validator.format("Pole może zawierać maksimum {0} znaki!")
			},
			fuel: {
				required: 'Pole wymagane',
				minlength: jQuery.validator.format("Pole musi zawierać minimum {0} znaki!"),
				maxlength: jQuery.validator.format("Pole może zawierać maksimum {0} znaki!")
			},
			colour: {
				required: 'Pole wymagane',
				minlength: jQuery.validator.format("Pole musi zawierać minimum {0} znaki!"),
				maxlength: jQuery.validator.format("Pole może zawierać maksimum {0} znaki!")
			},
			bodytype: {
				required: 'Pole wymagane'
			},
			owner: {
				required: 'Pole wymagane'
			}
			
		},
        highlight: function(element, errorClass, validClass) {
            $(element).closest('.form-group').addClass(errorClass).removeClass(validClass);
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).closest('.form-group').removeClass(errorClass).addClass(validClass);
        },
        errorClass: 'has-error has feedback',
		validClass: 'has-success',
		//umiejscowienie elementu z błędem
        errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
		/**
			niestadardowa rekacja na kliknięcie submit,
			gdy są błędy,
			blokuje standradową akcję
		**/
		invalidHandler: function(event, validator) {
			// 'this' to referencja do form
			var errors = validator.numberOfInvalids();
			if (errors) {
			  var message = errors == 1
				? 'Nie wypełniono poprawnie 1 pola. Zostało podświetlone'
				: 'Nie wypełniono poprawnie ' + errors + ' pól. Zostały podświetlone';
			  $("div.alert-danger").html(message);
			  $("div.alert-danger").show();
			} else {
			  $("div.alert-danger").hide();
			}
		},
	});
});
