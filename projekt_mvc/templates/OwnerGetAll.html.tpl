{extends file="Main.html.tpl"}
{block name=title}Project Car - lista właścicieli{/block}
{block name=body}
<div class="table-responsive container">
<div class="page-header">
	<br/>
    <h1>Lista właścicieli</h1>
</div>
{if isset($message)}
	<div class="alert alert-success" role="alert">{$message}</div>
{/if}      
{if isset($error)}
	<div class="alert alert-danger" role="alert">{$error}</div>
{/if}
	{if $owners|@count === 0}
	<div class="alert alert-info" role="alert">Brak rekordów w bazie!</div>
{else}
	<table id="data" class="table table-condensed table-striped" cellspacing="0" width="100%"> 
		<thead><tr>
             <th>id</th>
             <th>imię</th> 
			 <th>nazwisko</th>
			 <th>nr.telefonu</th> 			 
             <th>operacje</th> 
        </tr></thead>
		<tbody>
			{foreach $owners as $key => $owner} 
			<tr>
			<td>{$owner['idOwner']}</td>
			<td>{$owner['name']}</td>
			<td>{$owner['surname']}</td>
			<td>{$owner['phone']}</td>
			<td>
			<a type="button" class="btn btn-warning btn-xs" href="http://{$smarty.server.HTTP_HOST}{$subdir}owner/editform/{$owner['idOwner']}">edytuj</a>&nbsp;&nbsp;
			<a type="button" class="btn btn-danger btn-xs" href="http://{$smarty.server.HTTP_HOST}{$subdir}owner/delete/{$owner['idOwner']}">usuń</a>
			</td>
    </tr>
	{/foreach}
</tbody>
</table>
{/if}
		<br/>
		<a type="button" class="btn btn-primary btn-sm" href="http://{$smarty.server.HTTP_HOST}{$subdir}owner/addform/">Dodaj właściciela</a>
</div>
{/block}
