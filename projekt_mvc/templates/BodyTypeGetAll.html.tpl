{extends file="Main.html.tpl"}
{block name=title}Project Car - lista typów nadwozi{/block}
{block name=body}
<div class="table-responsive container">
<div class="page-header">
	<br/>
    <h1>Lista typów nadwozi</h1>
</div>
{if isset($message)}
	<div class="alert alert-success" role="alert">{$message}</div>
{/if}
{if isset($error)}
	<div class="alert alert-danger" role="alert">{$error}</div>
{/if}
	{if $bodytypes|@count === 0}
		<div class="alert alert-info" role="alert">Brak rekordów w bazie!</div>
	{else}
		<table id="data" class="table table-condensed table-striped" cellspacing="0" width="100%">
		<thead><tr>
             <th>id</th>
             <th>nazwa</th>
			 <th>samochody</th>
             <th>operacje</th>
        </tr></thead>
		<tbody>
			{foreach $bodytypes as $key => $bodytype}
			<tr id="{$bodytype['idBodyType']}">
            <td>{$bodytype['idBodyType']}</td>
            <td>{$bodytype['name']}</td>
			<td><a href="http://{$smarty.server.HTTP_HOST}{$subdir}car/intype/{$bodytype['idBodyType']}" data-toggle="tooltip" class="tip-right" data-original-title="Wyświetl wszystkie auta tego typu nadwozia"><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span>&nbsp;&nbsp;zobacz</a></td>
            <td>
			<a type="button" class="btn btn-warning btn-xs" href="http://{$smarty.server.HTTP_HOST}{$subdir}bodytype/editform/{$bodytype['idBodyType']}">edytuj</a>&nbsp;&nbsp;
            <a type="button" class="btn btn-danger btn-xs" href="http://{$smarty.server.HTTP_HOST}{$subdir}bodytype/delete/{$bodytype['idBodyType']}">usuń</a>
			</td>
        </tr>
        {/foreach}
    </tbody>
   </table>
{/if}
	<br/>
<!--    <a type="button" class="btn btn-primary btn-sm" href="http://{$smarty.server.HTTP_HOST}{$subdir}bodytype/addform/">Dodaj typ nadwozia</a>-->
    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#addModal">Dodaj typ nadwozia</button>
    <div id="addModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Typ nadwozia</h4>
          </div>
          <div class="modal-body">
            <form method="post" id="addForm">
                <div class="form-group">
                    <label for="name">Podaj typ nadwozia:</label>
                    <input type="text" class="form-control" id="addInput" autofocus="autofocus" name="name">
                </div>
            </form>
          </div>
          <div class="modal-footer">
              <button id="addBtn" class="btn btn-success">Dodaj</button>
          </div>
        </div>

      </div>
    </div>
</div>
{/block}
