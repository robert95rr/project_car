{extends file="Main.html.tpl"}
{block name=title}Project Car - lista samochodów{/block}
{block name=body}
<div class="container">
<div class="page-header">
<br/>
<h1>Dodaj samochód</h1>
</div>
<form id="carform" action="http://{$smarty.server.HTTP_HOST}{$subdir}car/add" method="post">
   <div class="form-group">
		<label for="brand">Marka:</label>
		<input type="text" class="form-control" id="brand" name="brand" autofocus="autofucus"><br />
   </div>
   <div class="form-group">
		<label for="model">Model:</label>
		<input type="text" class="form-control" id="model" name="model"><br />
   </div>
   <div class="form-group">
		<label for="yearProduction">Rok produkcji:</label>
		<input type="text" class="form-control" id="yearProduction" name="yearProduction"><br />
   </div>
   <div class="form-group">
		<label for="engineSize">Rozmiar silnika:</label> 
		<input type="text" class="form-control" id="engineSize" name="engineSize" placeholder="1900"><br />
   </div>
   <div class="form-group">
		<label for="enginePower">Moc silnika:</label>
		<input type="text" class="form-control" id="enginePower" name="enginePower"><br />
   </div>
   <div class="form-group">
		<label for="registrationNumber">Numer rejestracji:</label> 
		<input type="text" class="form-control" id="registrationNumber" name="registrationNumber" placeholder="PKA_45TR7"><br />
   </div>
   <div class="form-group">
		<label for="vinNumber">Numer VIN:</label>
		<input type="text" class="form-control" id="vinNumber" name="vinNumber"><br />
   </div>
   <div class="form-group">
		<label for="fuel">Paliwo:</label>
		<input type="text" class="form-control" id="fuel" name="fuel"><br />
   </div>
   <div class="form-group">
		<label for="colour">Kolor:</label> 
		<input type="text" class="form-control" id="colour" name="colour"><br />
   </div>
   <div class="form-group">
		<label for="bodytype">Typ nadwozia:</label>
		{html_options name="bodytype" options=$bodytypes class="form-control"}<br />
   </div>
   <div class="form-group">
		<label for="owner">Właściciel:</label>
		{html_options name=owner options=$owners class="form-control"}<br />
   </div>
	<div class="alert alert-danger collapse" role="alert"></div> 
    <button type="submit" class="btn btn-primary btn-sm">Dodaj</button>
</form>
</div>
{/block}