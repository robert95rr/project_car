{extends file="Main.html.tpl"}
{block name=title}Project Car - lista samochodów{/block}
{block name=body}
<div class="table-responsive container">
<div class="page-header">
	<br/>
    <h1>{block name=header}Lista samochodów{/block}</h1>
</div>
{if isset($message)}
	<div class="alert alert-success" role="alert">{$message}</div>
{/if}      
{if isset($error)}
	<div class="alert alert-danger" role="alert">{$error}</div>
{/if}
	{if $cars|@count === 0}
	<div class="alert alert-info" role="alert">Brak rekordów w bazie!</div>
{else}
	<table id="data" class="table table-condensed table-striped" cellspacing="0" width="100%"> 
		<thead><tr>
             <th>id</th>
             <th>marka</th> 
			 <th>model</th>
			 <th>rok produkcji</th> 
			 <th>poj. silnika[cm3]</th> 
			 <th>moc silnika[KM]</th> 
			 <th>nr.rejestracji</th> 
			 <th>numer VIN</th> 
			 <th>paliwo</th> 
			 <th>kolor</th> 
			 <th>typ nadwozia</th> 
			 <th>właściciel</th> 			 
             <th>operacje</th> 
        </tr></thead>
		<tbody>
			{foreach $cars as $key => $car} 
			<tr>
				<td>{$car['idCar']}</td>
				<td>{$car['brand']}</td>
				<td>{$car['model']}</td>
				<td align="center" valign="middle">{$car['yearProduction']}</td>
				<td align="center" valign="middle">{$car['engineSize']}</td>
				<td align="center" valign="middle">{$car['enginePower']}</td>
				<td>{$car['registrationNumber']}</td>
				<td>{$car['vinNumber']}</td>
				<td>{$car['fuel']}</td>
				<td>{$car['colour']}</td>
				<td align="center" valign="middle"><a href="http://{$smarty.server.HTTP_HOST}{$subdir}car/intype/{$car['idBodyType']}">{$car['bodytype_name']}</a></td>
				<td align="center" valign="middle"><a href="http://{$smarty.server.HTTP_HOST}{$subdir}car/owneris/{$car['idOwner']}">{$car['owner_name']}</a></td>		
				<td>
				<a type="button" class="btn btn-warning btn-xs" href="http://{$smarty.server.HTTP_HOST}{$subdir}car/editform/{$car['idCar']}">edytuj</a>
				<a type="button" class="btn btn-danger btn-xs" href="http://{$smarty.server.HTTP_HOST}{$subdir}car/delete/{$car['idCar']}">usuń</a>
				</td>
			</tr>
		{/foreach}
	</tbody>
	</table>
{/if}
	<br/>
	<a type="button"  class="btn btn-primary btn-sm" href="http://{$smarty.server.HTTP_HOST}{$subdir}car/addform/">Dodaj samochód</a>
</div>	
{/block}