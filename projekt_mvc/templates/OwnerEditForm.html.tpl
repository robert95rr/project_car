{extends file="Main.html.tpl"}
{block name=title}Project Car - lista właścicieli{/block}
{block name=body}
<div class="container">
<div class="page-header">
<br/>
<h1>Edytuj właściciela</h1>
</div>
<form id="ownerform" action="http://{$smarty.server.HTTP_HOST}{$subdir}owner/update" method="post">
  <input type="hidden" id="id" name="idOwner" value="{$idOwner}"> 
	<div class="form-group">
		<label for="name">Imię:</label>
		<input type="text" class="form-control" name="name" id="name" autofocus="autofucus" value="{$name}"><br />
	</div>
	<div class="form-group">
		<label for="surname">Nazwisko:</label>
		<input type="text" class="form-control" name="surname" id="surname" value="{$surname}"><br />
	</div>
	<div class="form-group">
		<label for="phone">Telefon:</label>
		<input type="tel" class="form-control" name="phone" id="phone" value="{$phone}">
	</div>
	<br />
	<div class="alert alert-danger collapse" role="alert"></div>
    <button type="submit" class="btn btn-primary btn-sm">Aktualizuj</button>
</form>
</div>
{/block}