{extends file="Main.html.tpl"}
{block name=title}Project Car - lista kont{/block}
{block name=body}
<div class="container">
<div class="page-header">
<br/>
<h1>Edytuj konto</h1>
</div>
<form id="accountform" action="http://{$smarty.server.HTTP_HOST}{$subdir}account/update" method="post">
    <input type="hidden" id="id" name="idAccount" value="{$idAccount}"> 
	<div class="form-group">
		<label for="login">Login:</label>
		<input type="text" class="form-control" id="login" name="login" autofocus="autofucus" value="{$login}"><br />
	</div>
	<div class="form-group">
		<label for="haslo">Hasło:</label>
		<input type="text" class="form-control" id="haslo" name="haslo" value="{$haslo}">
	</div>
		<br />
	<div class="alert alert-danger collapse" role="alert"></div>
    <button type="submit" class="btn btn-primary btn-sm">Aktualizuj</button>
</form>
</div>
{/block}