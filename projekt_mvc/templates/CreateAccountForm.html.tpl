{extends file="Main.html.tpl"}
{block name=title}Project Car - rejestracja{/block}
{block name=body}
<div class="container">
	<div class="page-header">
	<br/>
  		<h1>Rejestracja użytkowników</h1>
	</div>

    <form id="logform" action="http://{$smarty.server.HTTP_HOST}{$subdir}access/signup" method="post">
		<div class="form-group">
        	<label for="login">Login:</label>
        	<input type="text" class="form-control" id="login" name="login" autofocus="autofocus" placeholder="Wprowadź login">
		</div>
		<div class="form-group">
        	<label for="haslo">Hasło:</label>
			<input type="password" class="form-control" id="haslo" name="haslo" placeholder="Wprowadź hasło">
		</div>
		<div class="form-group">
			<progress id="passwordPr" value="0" max="100"></progress>
		</div>
		<div class="alert alert-danger collapse" role="alert"></div>
		{if isset($message)}
		<div class="alert alert-success" role="alert">{$message}</div>
		{/if}
		{if isset($error)}
		<div class="alert alert-danger" role="alert">{$error}</div>
		{/if}
		<button type="submit" class="btn btn-primary btn-sm">Zarejestruj</button>
    </form>
</div>
{/block}
