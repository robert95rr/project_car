{extends file="Main.html.tpl"}
{block name=title}Project Car - lista typów nadwozi{/block}
{block name=body}
<div class="container">
<div class="page-header">
<br/>
<h1>Dodaj typ nadwozia</h1>
</div>
<form id="bodytypeform" action="http://{$smarty.server.HTTP_HOST}{$subdir}bodytype/add" method="post">
	<div class="form-group">
	<label for="name">Nazwa typu nadwozia</label>
    <input type="text" class="form-control" id="name" name="name" autofocus="autofucus" placeholder="Wprowadź nazwę typu nadzwozia"><br />
	</div>
	<div class="alert alert-danger collapse" role="alert"></div> 
    <button type="submit" class="btn btn-primary btn-sm">Dodaj</button>
</form>
</div>
{/block}