{extends file="Main.html.tpl"}
{block name=title}Project Car - lista właścicieli{/block}
{block name=body}
<div class="container">
<div class="page-header">
<br/>
<h1>Dodaj właściciela</h1>
</div>
<form id="ownerform" action="http://{$smarty.server.HTTP_HOST}{$subdir}owner/add" method="post">
   <div class="form-group">
		<label for="name">Imię:</label>
		<input type="text" class="form-control" id="name" name="name" autofocus="autofucus"><br />
   </div>
   <div class="form-group">
		<label for="surname">Nazwisko:</label>
		<input type="text" class="form-control" id="surname" name="surname"><br />
   </div>
   <div class="form-group">
		<label for="phone">Telefon:</label>
		<input type="tel" class="form-control" id="phone" name="phone" placeholder="111-222-333">
   </div>
   <br />
   <div class="alert alert-danger collapse" role="alert"></div> 
    <button type="submit" class="btn btn-primary btn-sm">Dodaj</button>
</form>
</div>
{/block}