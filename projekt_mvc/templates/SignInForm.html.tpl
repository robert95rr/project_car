{extends file="Main.html.tpl"}
{block name=title}Projekt Car - logowanie{/block}
{block name=body}
<div class="container">
	<div class="page-header">
	<br/>
  		<h1>Zaloguj się</h1>
	</div>

    <form id="logform" action="http://{$smarty.server.HTTP_HOST}{$subdir}access/login" method="post">
		<div class="form-group">
        	<label for="name">Login:</label>
        	<input type="text" class="form-control" id="login" name="login" autofocus="autofocus" placeholder="Wprowadź login">
		</div>
		<div class="form-group">
        	<label for="name">Hasło:</label>
			<input type="password" class="form-control" id="haslo" name="haslo" placeholder="Wprowadź hasło">
		</div>
		<div class="alert alert-danger collapse" role="alert"></div>
		{if isset($message)}
		<div class="alert alert-success" role="alert">{$message}</div>
		{/if}
		{if isset($error)}
		<div class="alert alert-danger" role="alert">{$error}</div>
		{/if}
		<button type="submit" class="btn btn-primary btn-sm">Zaloguj</button>
    </form>
</div>
{/block}
