<!DOCTYPE html>
<html lang="pl">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{block name=title}Projekt Car{/block}</title>

	 <!-- Bootstrap -->
    <link href="http://{$smarty.server.HTTP_HOST}{$subdir}css/bootstrap.min.css" rel="stylesheet">
    
    <!-- DataTables -->
    <link href="http://{$smarty.server.HTTP_HOST}{$subdir}css/datatables.min.css" rel="stylesheet">      
    <link href="http://{$smarty.server.HTTP_HOST}{$subdir}css/style.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="http://{$smarty.server.HTTP_HOST}{$subdir}css/starter-template.css" rel="stylesheet">
	
  </head>
  <body>
  
   <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
		   <a class="navbar-brand" href="http://{$smarty.server.HTTP_HOST}{$subdir}index.php" style="color:#66ffcc">Projekt Car#</a>
        </div>
		<div id="navbar" class="collapse navbar-collapse">
         <ul class="nav navbar-nav">
			<li><a href="http://{$smarty.server.HTTP_HOST}{$subdir}account" style="color:#8FBC8F">Konta</a></li>
			<li><a href="http://{$smarty.server.HTTP_HOST}{$subdir}owner" style="color:#8FBC8F">Właściciele</a></li>
			<li><a href="http://{$smarty.server.HTTP_HOST}{$subdir}car" style="color:#8FBC8F">Samochody</a></li>
			<li><a href="http://{$smarty.server.HTTP_HOST}{$subdir}bodytype" style="color:#8FBC8F">Typy nadwozi</a></li>
		 </ul>
		 <ul class="nav navbar-nav navbar-right">
		  {if !isset($userid)}
            <li><a href="http://{$smarty.server.HTTP_HOST}{$subdir}access/logform" style="color:#66ffcc"><span class="glyphicon glyphicon-log-in" aria-hidden="true"></span> Zaloguj</a></li>
            <li><a href="http://{$smarty.server.HTTP_HOST}{$subdir}access/signupform" aria-hidden="true" style="color:#66ffcc"><span class="glyphicon glyphicon-user"></span> Rejestracja</a></li>
        {else}
            <li><a href="http://{$smarty.server.HTTP_HOST}{$subdir}access/logout" style="color:#66ffcc"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Wyloguj</a></li>
        {/if}
        </ul>
		</div><!--/.nav-collapse -->
	   </div>
	  </nav>
	  
			