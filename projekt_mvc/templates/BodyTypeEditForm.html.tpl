{extends file="Main.html.tpl"}
{block name=title}Project Car - lista typów nadwozi{/block}
{block name=body}
<div class="container">
<div class="page-header">
<br/>
<h1>Edytuj typ nadwozia</h1>
</div>
<form id="bodytypeform"action="http://{$smarty.server.HTTP_HOST}{$subdir}bodytype/update" method="post">
<input type="hidden" id="id" name="idBodyType" value="{$idBodyType}"> 
	<div class="form-group">
    <label for="name">Nazwa typu nadwozia:</label>
	<input type="text" class="form-control" name="name" id="bodytype" autofocus="autofucus" value="{$name}"><br />
	</div>
	<div class="alert alert-danger collapse" role="alert"></div>
    <button type="submit" class="btn btn-primary btn-sm">Aktualizuj</button>
</form>
</div>
{/block}