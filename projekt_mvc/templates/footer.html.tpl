<div class="container">
	  <!-- Site footer-->
	  <br/>
      <footer class="footer">
	  <hr>
		<p>Projekt Car by Robert Rezler 2017/18</p>
		<br/>
		<div class="progress progress-striped active">
			<div class="progress-bar progress-bar-info" style="width: 100%;color:#2F4F2F">Zapraszam!!!</div>
		</div>
		</footer>
	</div>
	
	 <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
	<script src="http://{$smarty.server.HTTP_HOST}{$subdir}js/jquery.min.js"></script>
	<script src="http://{$smarty.server.HTTP_HOST}{$subdir}js/jquery-ui.min.js"></script>
    <script src="http://{$smarty.server.HTTP_HOST}{$subdir}js/bootstrap.min.js"></script> 
	<script src="http://{$smarty.server.HTTP_HOST}{$subdir}js/jquery.cookie.js"></script>
	<script src="http://{$smarty.server.HTTP_HOST}{$subdir}js/custom.js"></script>
	<script src="http://{$smarty.server.HTTP_HOST}{$subdir}js/hintbodytype.js"></script>
	<script src="http://{$smarty.server.HTTP_HOST}{$subdir}js/power.js"></script>
	{if isset($customScript)}
        {foreach $customScript as $script}
            <script src="http://{$smarty.server.HTTP_HOST}{$subdir}js/{$script}.js"></script>
        {/foreach}
	{/if}
    </body>
</html>