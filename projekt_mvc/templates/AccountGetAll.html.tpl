{extends file="Main.html.tpl"}
{block name=title}Project Car - lista kont{/block}
{block name=body}
<div class="table-responsive container">
<div class="page-header">
	<br/>
    <h1>Lista kont</h1>
</div>
{if isset($message)}
	<div class="alert alert-success" role="alert">{$message}</div>
{/if}      
{if isset($error)}
	<div class="alert alert-danger" role="alert">{$error}</div>
{/if}
	{if $accounts|@count === 0}
    <div class="alert alert-info" role="alert">Brak rekordów w bazie!</div>
{else} 
	<table id="data" class="table table-condensed table-striped" cellspacing="0" width="100%"> 
		<thead><tr>
             <th>id</th>
             <th>login</th> 
			 <th>hasło</th>			 
             <th>operacje</th> 
        </tr></thead>
		<tbody>
        {foreach $accounts as $key => $account}
        <tr> 
		<td>{$account['idAccount']}</td>
        <td>{$account['login']}</td>
		<td>{$account['haslo']}</td>		
        <td>
		<a type="button" class="btn btn-warning btn-xs" href="http://{$smarty.server.HTTP_HOST}{$subdir}account/editform/{$account['idAccount']}">edytuj</a>&nbsp;&nbsp;
        <a type="button" class="btn btn-danger btn-xs" href="http://{$smarty.server.HTTP_HOST}{$subdir}account/delete/{$account['idAccount']}">usuń</a>
		</td>
    </tr>
	{/foreach}
</tbody>
</table>
{/if}
		<br/>
		{*<a type="button" class="btn btn-primary btn-sm" href="http://{$smarty.server.HTTP_HOST}{$subdir}account/addform/">Dodaj konto</a>*}
</div>
{/block}