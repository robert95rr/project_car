{extends file="Main.html.tpl"}
{block name=title}Project Car - lista kont{/block}
{block name=body}
<div class="container">
<div class="page-header">
<br/>
<h1>Dodaj konto</h1>
</div>
<form id="accountform" action="http://{$smarty.server.HTTP_HOST}{$subdir}account/add" method="post">
	<div class="form-group">
		<label for="login">Login:</label>
		<input type="text" class="form-control" id="login" name="login" autofocus="autofucus"><br />
   </div>
   <div class="form-group">
		<label for="haslo">Hasło:</label>
		<input type="text" class="form-control" id="haslo" name="haslo">
   </div>
	<br />
   <div class="alert alert-danger collapse" role="alert"></div> 
   <button type="submit" class="btn btn-primary btn-sm">Dodaj</button>
</form>
</div>
{/block}