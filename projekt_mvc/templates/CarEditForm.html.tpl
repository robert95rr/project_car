{extends file="Main.html.tpl"}
{block name=title}Project Car - lista samochodów{/block}
{block name=body}
<div class="container">
<div class="page-header">
<br/>
<h1>Edytuj samochód</h1>
</div>
<form id="carform" action="http://{$smarty.server.HTTP_HOST}{$subdir}car/update" method="post">
    <input type="hidden" id="id" name="idCar" value="{$idCar}"> 
	<div class="form-group">
		<label for="brand">Marka:</label>
		<input type="text" class="form-control" id="brand" name="brand" autofocus="autofucus" value="{$brand}"><br />
	</div>
	<div class="form-group">
		<label for="model">Model:</label>
		<input type="text" class="form-control" id="model" name="model" value="{$model}"><br />
	</div>
	<div class="form-group">
		<label for="yearProduction">Rok produkcji:</label>
		<input type="text" class="form-control" id="yearProduction" name="yearProduction" value="{$yearProduction}"><br />
	</div>
	<div class="form-group">
		<label for="engineSize">Rozmiar silnika:</label>
		<input type="text" class="form-control" id="engineSize" name="engineSize" value="{$engineSize}"><br />
	</div>
	<div class="form-group">
		<label for="enginePower">Moc silnika:</label>
		<input type="text" class="form-control" id="enginePower" name="enginePower" value="{$enginePower}"><br />
	</div>
	<div class="form-group">
		<label for="registrationNumber">Numer rejestracji:</label>
		<input type="text" class="form-control" id="registrationNumber" name="registrationNumber" value="{$registrationNumber}"><br />
	</div>
	<div class="form-group">
		<label for="vinNumber">Numer VIN: </label>
		<input type="text" class="form-control" id="vinNumber" name="vinNumber" value="{$vinNumber}"><br />
	</div>
	<div class="form-group">
		<label for="fuel">Paliwo:</label>
		<input type="text" class="form-control" id="fuel" name="fuel" value="{$fuel}"><br />
	</div>
	<div class="form-group">
		<label for="colour">Kolor:</label>
		<input type="text" class="form-control" id="colour" name="colour" value="{$colour}"><br />
	</div>
	<div class="form-group">
		<label for="bodytype">Typ nadwozia: </label>
		{html_options name=bodytype options=$bodytypes selected="{$idBodyType}" class="form-control"}<br />
	</div>
	<div class="form-group">
		<label for="owner">Właściciel: </label>
		{html_options name=owner options=$owners selected="{$idOwner}" class="form-control"}<br />
	</div>
	<div class="alert alert-danger collapse" role="alert"></div>
    <button type="submit" class="btn btn-primary btn-sm">Aktualizuj</button>
</form>
</div>
{/block}