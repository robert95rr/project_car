<?php
/* Smarty version 3.1.31, created on 2018-01-25 20:23:04
  from "C:\xampp\htdocs\Projekt_Car\projekt_mvc\templates\OwnerGetAll.html.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5a6a2e98d5ca30_94726253',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a6feb7963e77721e55e72d9579d6280d0db924ae' => 
    array (
      0 => 'C:\\xampp\\htdocs\\Projekt_Car\\projekt_mvc\\templates\\OwnerGetAll.html.tpl',
      1 => 1516908179,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a6a2e98d5ca30_94726253 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_142645a6a2e98d0e822_97845799', 'title');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_311215a6a2e98d126a8_60421418', 'body');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, "Main.html.tpl");
}
/* {block 'title'} */
class Block_142645a6a2e98d0e822_97845799 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'title' => 
  array (
    0 => 'Block_142645a6a2e98d0e822_97845799',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
Project Car - lista właścicieli<?php
}
}
/* {/block 'title'} */
/* {block 'body'} */
class Block_311215a6a2e98d126a8_60421418 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'body' => 
  array (
    0 => 'Block_311215a6a2e98d126a8_60421418',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="table-responsive container">
<div class="page-header">
	<br/>
    <h1>Lista właścicieli</h1>
</div>
<?php if (isset($_smarty_tpl->tpl_vars['message']->value)) {?>
	<div class="alert alert-success" role="alert"><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</div>
<?php }?>      
<?php if (isset($_smarty_tpl->tpl_vars['error']->value)) {?>
	<div class="alert alert-danger" role="alert"><?php echo $_smarty_tpl->tpl_vars['error']->value;?>
</div>
<?php }?>
	<?php if (count($_smarty_tpl->tpl_vars['owners']->value) === 0) {?>
	<div class="alert alert-info" role="alert">Brak rekordów w bazie!</div>
<?php } else { ?>
	<table id="data" class="table table-condensed table-striped" cellspacing="0" width="100%"> 
		<thead><tr>
             <th>id</th>
             <th>imię</th> 
			 <th>nazwisko</th>
			 <th>nr.telefonu</th> 			 
             <th>operacje</th> 
        </tr></thead>
		<tbody>
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['owners']->value, 'owner', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['owner']->value) {
?> 
			<tr>
			<td><?php echo $_smarty_tpl->tpl_vars['owner']->value['idOwner'];?>
</td>
			<td><?php echo $_smarty_tpl->tpl_vars['owner']->value['name'];?>
</td>
			<td><?php echo $_smarty_tpl->tpl_vars['owner']->value['surname'];?>
</td>
			<td><?php echo $_smarty_tpl->tpl_vars['owner']->value['phone'];?>
</td>
			<td>
			<a type="button" class="btn btn-warning btn-xs" href="http://<?php echo $_SERVER['HTTP_HOST'];
echo $_smarty_tpl->tpl_vars['subdir']->value;?>
owner/editform/<?php echo $_smarty_tpl->tpl_vars['owner']->value['idOwner'];?>
">edytuj</a>&nbsp;&nbsp;
			<a type="button" class="btn btn-danger btn-xs" href="http://<?php echo $_SERVER['HTTP_HOST'];
echo $_smarty_tpl->tpl_vars['subdir']->value;?>
owner/delete/<?php echo $_smarty_tpl->tpl_vars['owner']->value['idOwner'];?>
">usuń</a>
			</td>
    </tr>
	<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

</tbody>
</table>
<?php }?>
		<br/>
		<a type="button" class="btn btn-primary btn-sm" href="http://<?php echo $_SERVER['HTTP_HOST'];
echo $_smarty_tpl->tpl_vars['subdir']->value;?>
owner/addform/">Dodaj właściciela</a>
</div>
<?php
}
}
/* {/block 'body'} */
}
