<?php
/* Smarty version 3.1.31, created on 2018-01-08 18:48:12
  from "C:\xampp\htdocs\Projekt_Car\projekt_mvc\templates\OwnerEditForm.html.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5a53aedc33df80_83535145',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6571d328382a9f8059d7776b9cd5850636aa5657' => 
    array (
      0 => 'C:\\xampp\\htdocs\\Projekt_Car\\projekt_mvc\\templates\\OwnerEditForm.html.tpl',
      1 => 1515433686,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a53aedc33df80_83535145 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_33615a53aedc326880_22513473', 'title');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_218305a53aedc32a703_64859162', 'body');
$_smarty_tpl->inheritance->endChild($_smarty_tpl, "Main.html.tpl");
}
/* {block 'title'} */
class Block_33615a53aedc326880_22513473 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'title' => 
  array (
    0 => 'Block_33615a53aedc326880_22513473',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
Project Car - lista właścicieli<?php
}
}
/* {/block 'title'} */
/* {block 'body'} */
class Block_218305a53aedc32a703_64859162 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'body' => 
  array (
    0 => 'Block_218305a53aedc32a703_64859162',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="container">
<div class="page-header">
<br/>
<h1>Edytuj właściciela</h1>
</div>
<form id="ownerform" action="http://<?php echo $_SERVER['HTTP_HOST'];
echo $_smarty_tpl->tpl_vars['subdir']->value;?>
owner/update" method="post">
  <input type="hidden" id="id" name="idOwner" value="<?php echo $_smarty_tpl->tpl_vars['idOwner']->value;?>
"> 
	<div class="form-group">
		<label for="name">Imię:</label>
		<input type="text" class="form-control" name="name" id="name" autofocus="autofucus" value="<?php echo $_smarty_tpl->tpl_vars['name']->value;?>
"><br />
	</div>
	<div class="form-group">
		<label for="surname">Nazwisko:</label>
		<input type="text" class="form-control" name="surname" id="surname" value="<?php echo $_smarty_tpl->tpl_vars['surname']->value;?>
"><br />
	</div>
	<div class="form-group">
		<label for="phone">Telefon:</label>
		<input type="tel" class="form-control" name="phone" id="phone" value="<?php echo $_smarty_tpl->tpl_vars['phone']->value;?>
">
	</div>
	<br />
	<div class="alert alert-danger collapse" role="alert"></div>
    <button type="submit" class="btn btn-primary btn-sm">Aktualizuj</button>
</form>
</div>
<?php
}
}
/* {/block 'body'} */
}
