<?php
/* Smarty version 3.1.31, created on 2018-01-25 21:47:33
  from "C:\xampp\htdocs\Projekt_Car\projekt_mvc\templates\CarEditForm.html.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5a6a426529af15_05555053',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7942c44bfea9c2555bb2290b9e3e1ab675a70748' => 
    array (
      0 => 'C:\\xampp\\htdocs\\Projekt_Car\\projekt_mvc\\templates\\CarEditForm.html.tpl',
      1 => 1516913247,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a6a426529af15_05555053 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_html_options')) require_once 'C:\\xampp\\htdocs\\Projekt_Car\\projekt_mvc\\vendor\\smarty\\smarty\\libs\\plugins\\function.html_options.php';
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_115115a6a4265264403_14004221', 'title');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_59885a6a4265268280_01811809', 'body');
$_smarty_tpl->inheritance->endChild($_smarty_tpl, "Main.html.tpl");
}
/* {block 'title'} */
class Block_115115a6a4265264403_14004221 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'title' => 
  array (
    0 => 'Block_115115a6a4265264403_14004221',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
Project Car - lista samochodów<?php
}
}
/* {/block 'title'} */
/* {block 'body'} */
class Block_59885a6a4265268280_01811809 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'body' => 
  array (
    0 => 'Block_59885a6a4265268280_01811809',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="container">
<div class="page-header">
<br/>
<h1>Edytuj samochód</h1>
</div>
<form id="carform" action="http://<?php echo $_SERVER['HTTP_HOST'];
echo $_smarty_tpl->tpl_vars['subdir']->value;?>
car/update" method="post">
    <input type="hidden" id="id" name="idCar" value="<?php echo $_smarty_tpl->tpl_vars['idCar']->value;?>
"> 
	<div class="form-group">
		<label for="brand">Marka:</label>
		<input type="text" class="form-control" id="brand" name="brand" autofocus="autofucus" value="<?php echo $_smarty_tpl->tpl_vars['brand']->value;?>
"><br />
	</div>
	<div class="form-group">
		<label for="model">Model:</label>
		<input type="text" class="form-control" id="model" name="model" value="<?php echo $_smarty_tpl->tpl_vars['model']->value;?>
"><br />
	</div>
	<div class="form-group">
		<label for="yearProduction">Rok produkcji:</label>
		<input type="text" class="form-control" id="yearProduction" name="yearProduction" value="<?php echo $_smarty_tpl->tpl_vars['yearProduction']->value;?>
"><br />
	</div>
	<div class="form-group">
		<label for="engineSize">Rozmiar silnika:</label>
		<input type="text" class="form-control" id="engineSize" name="engineSize" value="<?php echo $_smarty_tpl->tpl_vars['engineSize']->value;?>
"><br />
	</div>
	<div class="form-group">
		<label for="enginePower">Moc silnika:</label>
		<input type="text" class="form-control" id="enginePower" name="enginePower" value="<?php echo $_smarty_tpl->tpl_vars['enginePower']->value;?>
"><br />
	</div>
	<div class="form-group">
		<label for="registrationNumber">Numer rejestracji:</label>
		<input type="text" class="form-control" id="registrationNumber" name="registrationNumber" value="<?php echo $_smarty_tpl->tpl_vars['registrationNumber']->value;?>
"><br />
	</div>
	<div class="form-group">
		<label for="vinNumber">Numer VIN: </label>
		<input type="text" class="form-control" id="vinNumber" name="vinNumber" value="<?php echo $_smarty_tpl->tpl_vars['vinNumber']->value;?>
"><br />
	</div>
	<div class="form-group">
		<label for="fuel">Paliwo:</label>
		<input type="text" class="form-control" id="fuel" name="fuel" value="<?php echo $_smarty_tpl->tpl_vars['fuel']->value;?>
"><br />
	</div>
	<div class="form-group">
		<label for="colour">Kolor:</label>
		<input type="text" class="form-control" id="colour" name="colour" value="<?php echo $_smarty_tpl->tpl_vars['colour']->value;?>
"><br />
	</div>
	<div class="form-group">
		<label for="bodytype">Typ nadwozia: </label>
		<?php echo smarty_function_html_options(array('name'=>'bodytype','options'=>$_smarty_tpl->tpl_vars['bodytypes']->value,'selected'=>((string)$_smarty_tpl->tpl_vars['idBodyType']->value),'class'=>"form-control"),$_smarty_tpl);?>
<br />
	</div>
	<div class="form-group">
		<label for="owner">Właściciel: </label>
		<?php echo smarty_function_html_options(array('name'=>'owner','options'=>$_smarty_tpl->tpl_vars['owners']->value,'selected'=>((string)$_smarty_tpl->tpl_vars['idOwner']->value),'class'=>"form-control"),$_smarty_tpl);?>
<br />
	</div>
	<div class="alert alert-danger collapse" role="alert"></div>
    <button type="submit" class="btn btn-primary btn-sm">Aktualizuj</button>
</form>
</div>
<?php
}
}
/* {/block 'body'} */
}
