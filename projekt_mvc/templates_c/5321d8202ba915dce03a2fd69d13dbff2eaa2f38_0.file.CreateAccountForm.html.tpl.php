<?php
/* Smarty version 3.1.31, created on 2018-01-26 11:32:43
  from "C:\xampp\htdocs\Projekt_Car\projekt_mvc\templates\CreateAccountForm.html.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5a6b03cb6b58e6_71057596',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5321d8202ba915dce03a2fd69d13dbff2eaa2f38' => 
    array (
      0 => 'C:\\xampp\\htdocs\\Projekt_Car\\projekt_mvc\\templates\\CreateAccountForm.html.tpl',
      1 => 1516962760,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a6b03cb6b58e6_71057596 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3215a6b03cb68e7e6_79011684', 'title');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_17665a6b03cb692667_54021921', 'body');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, "Main.html.tpl");
}
/* {block 'title'} */
class Block_3215a6b03cb68e7e6_79011684 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'title' => 
  array (
    0 => 'Block_3215a6b03cb68e7e6_79011684',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
Project Car - rejestracja<?php
}
}
/* {/block 'title'} */
/* {block 'body'} */
class Block_17665a6b03cb692667_54021921 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'body' => 
  array (
    0 => 'Block_17665a6b03cb692667_54021921',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="container">
	<div class="page-header">
	<br/>
  		<h1>Rejestracja użytkowników</h1>
	</div>

    <form id="logform" action="http://<?php echo $_SERVER['HTTP_HOST'];
echo $_smarty_tpl->tpl_vars['subdir']->value;?>
access/signup" method="post">
		<div class="form-group">
        	<label for="login">Login:</label>
        	<input type="text" class="form-control" id="login" name="login" autofocus="autofocus" placeholder="Wprowadź login">
		</div>
		<div class="form-group">
        	<label for="haslo">Hasło:</label>
			<input type="password" class="form-control" id="haslo" name="haslo" placeholder="Wprowadź hasło">
		</div>
		<div class="form-group">
			<progress id="passwordPr" value="0" max="100"></progress>
		</div>
		<div class="alert alert-danger collapse" role="alert"></div>
		<?php if (isset($_smarty_tpl->tpl_vars['message']->value)) {?>
		<div class="alert alert-success" role="alert"><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</div>
		<?php }?>
		<?php if (isset($_smarty_tpl->tpl_vars['error']->value)) {?>
		<div class="alert alert-danger" role="alert"><?php echo $_smarty_tpl->tpl_vars['error']->value;?>
</div>
		<?php }?>
		<button type="submit" class="btn btn-primary btn-sm">Zarejestruj</button>
    </form>
</div>
<?php
}
}
/* {/block 'body'} */
}
