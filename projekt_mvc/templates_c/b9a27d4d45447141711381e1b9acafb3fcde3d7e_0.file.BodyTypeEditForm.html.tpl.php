<?php
/* Smarty version 3.1.31, created on 2018-01-13 10:38:55
  from "C:\xampp\htdocs\Projekt_Car\projekt_mvc\templates\BodyTypeEditForm.html.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5a59d3af25bad3_93161282',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b9a27d4d45447141711381e1b9acafb3fcde3d7e' => 
    array (
      0 => 'C:\\xampp\\htdocs\\Projekt_Car\\projekt_mvc\\templates\\BodyTypeEditForm.html.tpl',
      1 => 1515501088,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a59d3af25bad3_93161282 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_4711145735a59d3af24fdc0_00723253', 'title');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18144689505a59d3af251316_49386849', 'body');
$_smarty_tpl->inheritance->endChild($_smarty_tpl, "Main.html.tpl");
}
/* {block 'title'} */
class Block_4711145735a59d3af24fdc0_00723253 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'title' => 
  array (
    0 => 'Block_4711145735a59d3af24fdc0_00723253',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
Project Car - lista typów nadwozi<?php
}
}
/* {/block 'title'} */
/* {block 'body'} */
class Block_18144689505a59d3af251316_49386849 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'body' => 
  array (
    0 => 'Block_18144689505a59d3af251316_49386849',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="container">
<div class="page-header">
<br/>
<h1>Edytuj typ nadwozia</h1>
</div>
<form id="bodytypeform"action="http://<?php echo $_SERVER['HTTP_HOST'];
echo $_smarty_tpl->tpl_vars['subdir']->value;?>
bodytype/update" method="post">
<input type="hidden" id="id" name="idBodyType" value="<?php echo $_smarty_tpl->tpl_vars['idBodyType']->value;?>
"> 
	<div class="form-group">
    <label for="name">Nazwa typu nadwozia:</label>
	<input type="text" class="form-control" name="name" id="bodytype" autofocus="autofucus" value="<?php echo $_smarty_tpl->tpl_vars['name']->value;?>
"><br />
	</div>
	<div class="alert alert-danger collapse" role="alert"></div>
    <button type="submit" class="btn btn-primary btn-sm">Aktualizuj</button>
</form>
</div>
<?php
}
}
/* {/block 'body'} */
}
