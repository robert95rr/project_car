<?php
/* Smarty version 3.1.31, created on 2018-01-10 12:34:40
  from "C:\xampp\htdocs\Projekt_Car\projekt_mvc\templates\CarAddForm.html.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5a55fa50e74143_85617390',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c73f8b45d7c1fb14a1f4fa9c3b45e7f4424586f7' => 
    array (
      0 => 'C:\\xampp\\htdocs\\Projekt_Car\\projekt_mvc\\templates\\CarAddForm.html.tpl',
      1 => 1515584063,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a55fa50e74143_85617390 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_html_options')) require_once 'C:\\xampp\\htdocs\\Projekt_Car\\projekt_mvc\\vendor\\smarty\\smarty\\libs\\plugins\\function.html_options.php';
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_151825a55fa50e35932_47633641', 'title');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_134235a55fa50e397b7_10503866', 'body');
$_smarty_tpl->inheritance->endChild($_smarty_tpl, "Main.html.tpl");
}
/* {block 'title'} */
class Block_151825a55fa50e35932_47633641 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'title' => 
  array (
    0 => 'Block_151825a55fa50e35932_47633641',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
Project Car - lista samochodów<?php
}
}
/* {/block 'title'} */
/* {block 'body'} */
class Block_134235a55fa50e397b7_10503866 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'body' => 
  array (
    0 => 'Block_134235a55fa50e397b7_10503866',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="container">
<div class="page-header">
<br/>
<h1>Dodaj samochód</h1>
</div>
<form id="carform" action="http://<?php echo $_SERVER['HTTP_HOST'];
echo $_smarty_tpl->tpl_vars['subdir']->value;?>
car/add" method="post">
   <div class="form-group">
		<label for="brand">Marka:</label>
		<input type="text" class="form-control" id="brand" name="brand" autofocus="autofucus"><br />
   </div>
   <div class="form-group">
		<label for="model">Model:</label>
		<input type="text" class="form-control" id="model" name="model"><br />
   </div>
   <div class="form-group">
		<label for="yearProduction">Rok produkcji:</label>
		<input type="text" class="form-control" id="yearProduction" name="yearProduction"><br />
   </div>
   <div class="form-group">
		<label for="engineSize">Rozmiar silnika:</label> 
		<input type="text" class="form-control" id="engineSize" name="engineSize" placeholder="1900"><br />
   </div>
   <div class="form-group">
		<label for="enginePower">Moc silnika:</label>
		<input type="text" class="form-control" id="enginePower" name="enginePower"><br />
   </div>
   <div class="form-group">
		<label for="registrationNumber">Numer rejestracji:</label> 
		<input type="text" class="form-control" id="registrationNumber" name="registrationNumber" placeholder="PKA_45TR7"><br />
   </div>
   <div class="form-group">
		<label for="vinNumber">Numer VIN:</label>
		<input type="text" class="form-control" id="vinNumber" name="vinNumber"><br />
   </div>
   <div class="form-group">
		<label for="fuel">Paliwo:</label>
		<input type="text" class="form-control" id="fuel" name="fuel"><br />
   </div>
   <div class="form-group">
		<label for="colour">Kolor:</label> 
		<input type="text" class="form-control" id="colour" name="colour"><br />
   </div>
   <div class="form-group">
		<label for="bodytype">Typ nadwozia:</label>
		<?php echo smarty_function_html_options(array('name'=>"bodytype",'options'=>$_smarty_tpl->tpl_vars['bodytypes']->value,'class'=>"form-control"),$_smarty_tpl);?>
<br />
   </div>
   <div class="form-group">
		<label for="owner">Właściciel:</label>
		<?php echo smarty_function_html_options(array('name'=>'owner','options'=>$_smarty_tpl->tpl_vars['owners']->value,'class'=>"form-control"),$_smarty_tpl);?>
<br />
   </div>
	<div class="alert alert-danger collapse" role="alert"></div> 
    <button type="submit" class="btn btn-primary btn-sm">Dodaj</button>
</form>
</div>
<?php
}
}
/* {/block 'body'} */
}
