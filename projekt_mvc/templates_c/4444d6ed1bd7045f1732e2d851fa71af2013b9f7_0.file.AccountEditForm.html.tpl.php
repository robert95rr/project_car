<?php
/* Smarty version 3.1.31, created on 2018-01-10 21:49:03
  from "C:\xampp\htdocs\Projekt_Car\projekt_mvc\templates\AccountEditForm.html.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5a567c3f7fca68_95003592',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4444d6ed1bd7045f1732e2d851fa71af2013b9f7' => 
    array (
      0 => 'C:\\xampp\\htdocs\\Projekt_Car\\projekt_mvc\\templates\\AccountEditForm.html.tpl',
      1 => 1515446546,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a567c3f7fca68_95003592 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_232265a567c3f7e5369_54644538', 'title');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_321075a567c3f7e91e2_96069515', 'body');
$_smarty_tpl->inheritance->endChild($_smarty_tpl, "Main.html.tpl");
}
/* {block 'title'} */
class Block_232265a567c3f7e5369_54644538 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'title' => 
  array (
    0 => 'Block_232265a567c3f7e5369_54644538',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
Project Car - lista kont<?php
}
}
/* {/block 'title'} */
/* {block 'body'} */
class Block_321075a567c3f7e91e2_96069515 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'body' => 
  array (
    0 => 'Block_321075a567c3f7e91e2_96069515',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="container">
<div class="page-header">
<br/>
<h1>Edytuj konto</h1>
</div>
<form id="accountform" action="http://<?php echo $_SERVER['HTTP_HOST'];
echo $_smarty_tpl->tpl_vars['subdir']->value;?>
account/update" method="post">
    <input type="hidden" id="id" name="idAccount" value="<?php echo $_smarty_tpl->tpl_vars['idAccount']->value;?>
"> 
	<div class="form-group">
		<label for="login">Login:</label>
		<input type="text" class="form-control" id="login" name="login" autofocus="autofucus" value="<?php echo $_smarty_tpl->tpl_vars['login']->value;?>
"><br />
	</div>
	<div class="form-group">
		<label for="haslo">Hasło:</label>
		<input type="text" class="form-control" id="haslo" name="haslo" value="<?php echo $_smarty_tpl->tpl_vars['haslo']->value;?>
">
	</div>
		<br />
	<div class="alert alert-danger collapse" role="alert"></div>
    <button type="submit" class="btn btn-primary btn-sm">Aktualizuj</button>
</form>
</div>
<?php
}
}
/* {/block 'body'} */
}
