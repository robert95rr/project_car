<?php
/* Smarty version 3.1.31, created on 2018-01-08 19:54:54
  from "C:\xampp\htdocs\Projekt_Car\projekt_mvc\templates\OwnerAddForm.html.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5a53be7eac45c4_61053097',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ea31d98347e0499421a27aa02448029043952304' => 
    array (
      0 => 'C:\\xampp\\htdocs\\Projekt_Car\\projekt_mvc\\templates\\OwnerAddForm.html.tpl',
      1 => 1515437457,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a53be7eac45c4_61053097 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_273585a53be7eab0d41_81423422', 'title');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_216955a53be7eab0d43_44901407', 'body');
$_smarty_tpl->inheritance->endChild($_smarty_tpl, "Main.html.tpl");
}
/* {block 'title'} */
class Block_273585a53be7eab0d41_81423422 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'title' => 
  array (
    0 => 'Block_273585a53be7eab0d41_81423422',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
Project Car - lista właścicieli<?php
}
}
/* {/block 'title'} */
/* {block 'body'} */
class Block_216955a53be7eab0d43_44901407 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'body' => 
  array (
    0 => 'Block_216955a53be7eab0d43_44901407',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="container">
<div class="page-header">
<br/>
<h1>Dodaj właściciela</h1>
</div>
<form id="ownerform" action="http://<?php echo $_SERVER['HTTP_HOST'];
echo $_smarty_tpl->tpl_vars['subdir']->value;?>
owner/add" method="post">
   <div class="form-group">
		<label for="name">Imię:</label>
		<input type="text" class="form-control" id="name" name="name" autofocus="autofucus"><br />
   </div>
   <div class="form-group">
		<label for="surname">Nazwisko:</label>
		<input type="text" class="form-control" id="surname" name="surname"><br />
   </div>
   <div class="form-group">
		<label for="phone">Telefon:</label>
		<input type="tel" class="form-control" id="phone" name="phone" placeholder="111-222-333">
   </div>
   <br />
   <div class="alert alert-danger collapse" role="alert"></div> 
    <button type="submit" class="btn btn-primary btn-sm">Dodaj</button>
</form>
</div>
<?php
}
}
/* {/block 'body'} */
}
