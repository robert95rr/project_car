<?php
/* Smarty version 3.1.31, created on 2018-01-09 13:34:35
  from "C:\xampp\htdocs\Projekt_Car\projekt_mvc\templates\BodyTypeAddForm.html.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5a54b6dbedcdb6_61282948',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '35f5f6f281f9163e52454c1b5f37cdcb5fb0bbff' => 
    array (
      0 => 'C:\\xampp\\htdocs\\Projekt_Car\\projekt_mvc\\templates\\BodyTypeAddForm.html.tpl',
      1 => 1515501075,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a54b6dbedcdb6_61282948 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_169625a54b6dbec9523_23470321', 'title');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_5955a54b6dbecd3a1_30027213', 'body');
$_smarty_tpl->inheritance->endChild($_smarty_tpl, "Main.html.tpl");
}
/* {block 'title'} */
class Block_169625a54b6dbec9523_23470321 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'title' => 
  array (
    0 => 'Block_169625a54b6dbec9523_23470321',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
Project Car - lista typów nadwozi<?php
}
}
/* {/block 'title'} */
/* {block 'body'} */
class Block_5955a54b6dbecd3a1_30027213 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'body' => 
  array (
    0 => 'Block_5955a54b6dbecd3a1_30027213',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="container">
<div class="page-header">
<br/>
<h1>Dodaj typ nadwozia</h1>
</div>
<form id="bodytypeform" action="http://<?php echo $_SERVER['HTTP_HOST'];
echo $_smarty_tpl->tpl_vars['subdir']->value;?>
bodytype/add" method="post">
	<div class="form-group">
	<label for="name">Nazwa typu nadwozia</label>
    <input type="text" class="form-control" id="name" name="name" autofocus="autofucus" placeholder="Wprowadź nazwę typu nadzwozia"><br />
	</div>
	<div class="alert alert-danger collapse" role="alert"></div> 
    <button type="submit" class="btn btn-primary btn-sm">Dodaj</button>
</form>
</div>
<?php
}
}
/* {/block 'body'} */
}
