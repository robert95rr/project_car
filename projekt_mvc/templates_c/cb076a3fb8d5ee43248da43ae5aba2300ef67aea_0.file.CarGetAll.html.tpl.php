<?php
/* Smarty version 3.1.31, created on 2018-01-25 20:24:06
  from "C:\xampp\htdocs\Projekt_Car\projekt_mvc\templates\CarGetAll.html.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5a6a2ed643ed55_33064460',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'cb076a3fb8d5ee43248da43ae5aba2300ef67aea' => 
    array (
      0 => 'C:\\xampp\\htdocs\\Projekt_Car\\projekt_mvc\\templates\\CarGetAll.html.tpl',
      1 => 1516908241,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a6a2ed643ed55_33064460 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_233655a6a2ed63c5bc6_36987664', 'title');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_47625a6a2ed63c9a47_66082744', 'body');
$_smarty_tpl->inheritance->endChild($_smarty_tpl, "Main.html.tpl");
}
/* {block 'title'} */
class Block_233655a6a2ed63c5bc6_36987664 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'title' => 
  array (
    0 => 'Block_233655a6a2ed63c5bc6_36987664',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
Project Car - lista samochodów<?php
}
}
/* {/block 'title'} */
/* {block 'header'} */
class Block_19955a6a2ed63c9a48_41189193 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
Lista samochodów<?php
}
}
/* {/block 'header'} */
/* {block 'body'} */
class Block_47625a6a2ed63c9a47_66082744 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'body' => 
  array (
    0 => 'Block_47625a6a2ed63c9a47_66082744',
  ),
  'header' => 
  array (
    0 => 'Block_19955a6a2ed63c9a48_41189193',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="table-responsive container">
<div class="page-header">
	<br/>
    <h1><?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_19955a6a2ed63c9a48_41189193', 'header', $this->tplIndex);
?>
</h1>
</div>
<?php if (isset($_smarty_tpl->tpl_vars['message']->value)) {?>
	<div class="alert alert-success" role="alert"><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</div>
<?php }?>      
<?php if (isset($_smarty_tpl->tpl_vars['error']->value)) {?>
	<div class="alert alert-danger" role="alert"><?php echo $_smarty_tpl->tpl_vars['error']->value;?>
</div>
<?php }?>
	<?php if (count($_smarty_tpl->tpl_vars['cars']->value) === 0) {?>
	<div class="alert alert-info" role="alert">Brak rekordów w bazie!</div>
<?php } else { ?>
	<table id="data" class="table table-condensed table-striped" cellspacing="0" width="100%"> 
		<thead><tr>
             <th>id</th>
             <th>marka</th> 
			 <th>model</th>
			 <th>rok produkcji</th> 
			 <th>poj. silnika[cm3]</th> 
			 <th>moc silnika[KM]</th> 
			 <th>nr.rejestracji</th> 
			 <th>numer VIN</th> 
			 <th>paliwo</th> 
			 <th>kolor</th> 
			 <th>typ nadwozia</th> 
			 <th>właściciel</th> 			 
             <th>operacje</th> 
        </tr></thead>
		<tbody>
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cars']->value, 'car', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['car']->value) {
?> 
			<tr>
				<td><?php echo $_smarty_tpl->tpl_vars['car']->value['idCar'];?>
</td>
				<td><?php echo $_smarty_tpl->tpl_vars['car']->value['brand'];?>
</td>
				<td><?php echo $_smarty_tpl->tpl_vars['car']->value['model'];?>
</td>
				<td align="center" valign="middle"><?php echo $_smarty_tpl->tpl_vars['car']->value['yearProduction'];?>
</td>
				<td align="center" valign="middle"><?php echo $_smarty_tpl->tpl_vars['car']->value['engineSize'];?>
</td>
				<td align="center" valign="middle"><?php echo $_smarty_tpl->tpl_vars['car']->value['enginePower'];?>
</td>
				<td><?php echo $_smarty_tpl->tpl_vars['car']->value['registrationNumber'];?>
</td>
				<td><?php echo $_smarty_tpl->tpl_vars['car']->value['vinNumber'];?>
</td>
				<td><?php echo $_smarty_tpl->tpl_vars['car']->value['fuel'];?>
</td>
				<td><?php echo $_smarty_tpl->tpl_vars['car']->value['colour'];?>
</td>
				<td align="center" valign="middle"><a href="http://<?php echo $_SERVER['HTTP_HOST'];
echo $_smarty_tpl->tpl_vars['subdir']->value;?>
car/intype/<?php echo $_smarty_tpl->tpl_vars['car']->value['idBodyType'];?>
"><?php echo $_smarty_tpl->tpl_vars['car']->value['bodytype_name'];?>
</a></td>
				<td align="center" valign="middle"><a href="http://<?php echo $_SERVER['HTTP_HOST'];
echo $_smarty_tpl->tpl_vars['subdir']->value;?>
car/owneris/<?php echo $_smarty_tpl->tpl_vars['car']->value['idOwner'];?>
"><?php echo $_smarty_tpl->tpl_vars['car']->value['owner_name'];?>
</a></td>		
				<td>
				<a type="button" class="btn btn-warning btn-xs" href="http://<?php echo $_SERVER['HTTP_HOST'];
echo $_smarty_tpl->tpl_vars['subdir']->value;?>
car/editform/<?php echo $_smarty_tpl->tpl_vars['car']->value['idCar'];?>
">edytuj</a>
				<a type="button" class="btn btn-danger btn-xs" href="http://<?php echo $_SERVER['HTTP_HOST'];
echo $_smarty_tpl->tpl_vars['subdir']->value;?>
car/delete/<?php echo $_smarty_tpl->tpl_vars['car']->value['idCar'];?>
">usuń</a>
				</td>
			</tr>
		<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

	</tbody>
	</table>
<?php }?>
	<br/>
	<a type="button"  class="btn btn-primary btn-sm" href="http://<?php echo $_SERVER['HTTP_HOST'];
echo $_smarty_tpl->tpl_vars['subdir']->value;?>
car/addform/">Dodaj samochód</a>
</div>	
<?php
}
}
/* {/block 'body'} */
}
