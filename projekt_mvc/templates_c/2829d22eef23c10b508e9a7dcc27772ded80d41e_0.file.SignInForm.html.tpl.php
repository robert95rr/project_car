<?php
/* Smarty version 3.1.31, created on 2018-01-10 19:04:33
  from "C:\xampp\htdocs\Projekt_Car\projekt_mvc\templates\SignInForm.html.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5a5655b18b3ee1_01591400',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2829d22eef23c10b508e9a7dcc27772ded80d41e' => 
    array (
      0 => 'C:\\xampp\\htdocs\\Projekt_Car\\projekt_mvc\\templates\\SignInForm.html.tpl',
      1 => 1515607459,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a5655b18b3ee1_01591400 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_209105a5655b1894ad2_38827646', 'title');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_315675a5655b1898958_84177362', 'body');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, "Main.html.tpl");
}
/* {block 'title'} */
class Block_209105a5655b1894ad2_38827646 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'title' => 
  array (
    0 => 'Block_209105a5655b1894ad2_38827646',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
Projekt Car - logowanie<?php
}
}
/* {/block 'title'} */
/* {block 'body'} */
class Block_315675a5655b1898958_84177362 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'body' => 
  array (
    0 => 'Block_315675a5655b1898958_84177362',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="container">
	<div class="page-header">
	<br/>
  		<h1>Zaloguj się</h1>
	</div>

    <form id="logform" action="http://<?php echo $_SERVER['HTTP_HOST'];
echo $_smarty_tpl->tpl_vars['subdir']->value;?>
access/login" method="post">
		<div class="form-group">
        	<label for="name">Login:</label>
        	<input type="text" class="form-control" id="login" name="login" autofocus="autofocus" placeholder="Wprowadź login">
		</div>
		<div class="form-group">
        	<label for="name">Hasło:</label>
			<input type="password" class="form-control" id="haslo" name="haslo" placeholder="Wprowadź hasło">
		</div>
		<div class="alert alert-danger collapse" role="alert"></div>
		<?php if (isset($_smarty_tpl->tpl_vars['message']->value)) {?>
		<div class="alert alert-success" role="alert"><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</div>
		<?php }?>
		<?php if (isset($_smarty_tpl->tpl_vars['error']->value)) {?>
		<div class="alert alert-danger" role="alert"><?php echo $_smarty_tpl->tpl_vars['error']->value;?>
</div>
		<?php }?>
		<button type="submit" class="btn btn-primary btn-sm">Zaloguj</button>
    </form>
</div>
<?php
}
}
/* {/block 'body'} */
}
