<?php
/* Smarty version 3.1.31, created on 2019-11-15 17:41:22
  from "C:\xampp\htdocs\Projekt_Car\projekt_mvc\templates\BodyTypeGetAll.html.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5dced5327dead1_10974624',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'cd05387e759162d76a56a1f9883a6a47d423df6c' => 
    array (
      0 => 'C:\\xampp\\htdocs\\Projekt_Car\\projekt_mvc\\templates\\BodyTypeGetAll.html.tpl',
      1 => 1573835147,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5dced5327dead1_10974624 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12560675885dced5326ca5a0_37416008', 'title');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15800037845dced5326cc024_59858265', 'body');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, "Main.html.tpl");
}
/* {block 'title'} */
class Block_12560675885dced5326ca5a0_37416008 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'title' => 
  array (
    0 => 'Block_12560675885dced5326ca5a0_37416008',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
Project Car - lista typów nadwozi<?php
}
}
/* {/block 'title'} */
/* {block 'body'} */
class Block_15800037845dced5326cc024_59858265 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'body' => 
  array (
    0 => 'Block_15800037845dced5326cc024_59858265',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="table-responsive container">
<div class="page-header">
	<br/>
    <h1>Lista typów nadwozi</h1>
</div>
<?php if (isset($_smarty_tpl->tpl_vars['message']->value)) {?>
	<div class="alert alert-success" role="alert"><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</div>
<?php }
if (isset($_smarty_tpl->tpl_vars['error']->value)) {?>
	<div class="alert alert-danger" role="alert"><?php echo $_smarty_tpl->tpl_vars['error']->value;?>
</div>
<?php }?>
	<?php if (count($_smarty_tpl->tpl_vars['bodytypes']->value) === 0) {?>
		<div class="alert alert-info" role="alert">Brak rekordów w bazie!</div>
	<?php } else { ?>
		<table id="data" class="table table-condensed table-striped" cellspacing="0" width="100%">
		<thead><tr>
             <th>id</th>
             <th>nazwa</th>
			 <th>samochody</th>
             <th>operacje</th>
        </tr></thead>
		<tbody>
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['bodytypes']->value, 'bodytype', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['bodytype']->value) {
?>
			<tr id="<?php echo $_smarty_tpl->tpl_vars['bodytype']->value['idBodyType'];?>
">
            <td><?php echo $_smarty_tpl->tpl_vars['bodytype']->value['idBodyType'];?>
</td>
            <td><?php echo $_smarty_tpl->tpl_vars['bodytype']->value['name'];?>
</td>
			<td><a href="http://<?php echo $_SERVER['HTTP_HOST'];
echo $_smarty_tpl->tpl_vars['subdir']->value;?>
car/intype/<?php echo $_smarty_tpl->tpl_vars['bodytype']->value['idBodyType'];?>
" data-toggle="tooltip" class="tip-right" data-original-title="Wyświetl wszystkie auta tego typu nadwozia"><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span>&nbsp;&nbsp;zobacz</a></td>
            <td>
			<a type="button" class="btn btn-warning btn-xs" href="http://<?php echo $_SERVER['HTTP_HOST'];
echo $_smarty_tpl->tpl_vars['subdir']->value;?>
bodytype/editform/<?php echo $_smarty_tpl->tpl_vars['bodytype']->value['idBodyType'];?>
">edytuj</a>&nbsp;&nbsp;
            <a type="button" class="btn btn-danger btn-xs" href="http://<?php echo $_SERVER['HTTP_HOST'];
echo $_smarty_tpl->tpl_vars['subdir']->value;?>
bodytype/delete/<?php echo $_smarty_tpl->tpl_vars['bodytype']->value['idBodyType'];?>
">usuń</a>
			</td>
        </tr>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

    </tbody>
   </table>
<?php }?>
	<br/>
<!--    <a type="button" class="btn btn-primary btn-sm" href="http://<?php echo $_SERVER['HTTP_HOST'];
echo $_smarty_tpl->tpl_vars['subdir']->value;?>
bodytype/addform/">Dodaj typ nadwozia</a>-->
    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#addModal">Dodaj typ nadwozia</button>
    <div id="addModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Typ nadwozia</h4>
          </div>
          <div class="modal-body">
            <form method="post" id="addForm">
                <div class="form-group">
                    <label for="name">Podaj typ nadwozia:</label>
                    <input type="text" class="form-control" id="addInput" autofocus="autofocus" name="name">
                </div>
            </form>
          </div>
          <div class="modal-footer">
              <button id="addBtn" class="btn btn-success">Dodaj</button>
          </div>
        </div>

      </div>
    </div>
</div>
<?php
}
}
/* {/block 'body'} */
}
