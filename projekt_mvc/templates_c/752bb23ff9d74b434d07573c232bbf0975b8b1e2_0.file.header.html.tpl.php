<?php
/* Smarty version 3.1.31, created on 2018-01-25 23:44:37
  from "C:\xampp\htdocs\Projekt_Car\projekt_mvc\templates\header.html.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5a6a5dd50a87e3_65076487',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '752bb23ff9d74b434d07573c232bbf0975b8b1e2' => 
    array (
      0 => 'C:\\xampp\\htdocs\\Projekt_Car\\projekt_mvc\\templates\\header.html.tpl',
      1 => 1516920268,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a6a5dd50a87e3_65076487 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>
<!DOCTYPE html>
<html lang="pl">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_165215a6a5dd5066158_63279863', 'title');
?>
</title>

	 <!-- Bootstrap -->
    <link href="http://<?php echo $_SERVER['HTTP_HOST'];
echo $_smarty_tpl->tpl_vars['subdir']->value;?>
css/bootstrap.min.css" rel="stylesheet">
    
    <!-- DataTables -->
    <link href="http://<?php echo $_SERVER['HTTP_HOST'];
echo $_smarty_tpl->tpl_vars['subdir']->value;?>
css/datatables.min.css" rel="stylesheet">      
    <link href="http://<?php echo $_SERVER['HTTP_HOST'];
echo $_smarty_tpl->tpl_vars['subdir']->value;?>
css/style.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="http://<?php echo $_SERVER['HTTP_HOST'];
echo $_smarty_tpl->tpl_vars['subdir']->value;?>
css/starter-template.css" rel="stylesheet">
	
  </head>
  <body>
  
   <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
		   <a class="navbar-brand" href="http://<?php echo $_SERVER['HTTP_HOST'];
echo $_smarty_tpl->tpl_vars['subdir']->value;?>
index.php" style="color:#66ffcc">Projekt Car#</a>
        </div>
		<div id="navbar" class="collapse navbar-collapse">
         <ul class="nav navbar-nav">
			<li><a href="http://<?php echo $_SERVER['HTTP_HOST'];
echo $_smarty_tpl->tpl_vars['subdir']->value;?>
account" style="color:#8FBC8F">Konta</a></li>
			<li><a href="http://<?php echo $_SERVER['HTTP_HOST'];
echo $_smarty_tpl->tpl_vars['subdir']->value;?>
owner" style="color:#8FBC8F">Właściciele</a></li>
			<li><a href="http://<?php echo $_SERVER['HTTP_HOST'];
echo $_smarty_tpl->tpl_vars['subdir']->value;?>
car" style="color:#8FBC8F">Samochody</a></li>
			<li><a href="http://<?php echo $_SERVER['HTTP_HOST'];
echo $_smarty_tpl->tpl_vars['subdir']->value;?>
bodytype" style="color:#8FBC8F">Typy nadwozi</a></li>
		 </ul>
		 <ul class="nav navbar-nav navbar-right">
		  <?php if (!isset($_smarty_tpl->tpl_vars['userid']->value)) {?>
            <li><a href="http://<?php echo $_SERVER['HTTP_HOST'];
echo $_smarty_tpl->tpl_vars['subdir']->value;?>
access/logform" style="color:#66ffcc"><span class="glyphicon glyphicon-log-in" aria-hidden="true"></span> Zaloguj</a></li>
            <li><a href="http://<?php echo $_SERVER['HTTP_HOST'];
echo $_smarty_tpl->tpl_vars['subdir']->value;?>
access/signupform" aria-hidden="true" style="color:#66ffcc"><span class="glyphicon glyphicon-user"></span> Rejestracja</a></li>
        <?php } else { ?>
            <li><a href="http://<?php echo $_SERVER['HTTP_HOST'];
echo $_smarty_tpl->tpl_vars['subdir']->value;?>
access/logout" style="color:#66ffcc"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Wyloguj</a></li>
        <?php }?>
        </ul>
		</div><!--/.nav-collapse -->
	   </div>
	  </nav>
	  
			<?php }
/* {block 'title'} */
class Block_165215a6a5dd5066158_63279863 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'title' => 
  array (
    0 => 'Block_165215a6a5dd5066158_63279863',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
Projekt Car<?php
}
}
/* {/block 'title'} */
}
