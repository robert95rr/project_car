<?php
/* Smarty version 3.1.31, created on 2018-01-25 20:19:44
  from "C:\xampp\htdocs\Projekt_Car\projekt_mvc\templates\AccountGetAll.html.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5a6a2dd073e669_92043167',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7be31ef7759cf49c96f9a198c7e2d04b0150a8db' => 
    array (
      0 => 'C:\\xampp\\htdocs\\Projekt_Car\\projekt_mvc\\templates\\AccountGetAll.html.tpl',
      1 => 1516907978,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a6a2dd073e669_92043167 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_212245a6a2dd06f42d6_16953951', 'title');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_115215a6a2dd06f8157_80629159', 'body');
$_smarty_tpl->inheritance->endChild($_smarty_tpl, "Main.html.tpl");
}
/* {block 'title'} */
class Block_212245a6a2dd06f42d6_16953951 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'title' => 
  array (
    0 => 'Block_212245a6a2dd06f42d6_16953951',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
Project Car - lista kont<?php
}
}
/* {/block 'title'} */
/* {block 'body'} */
class Block_115215a6a2dd06f8157_80629159 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'body' => 
  array (
    0 => 'Block_115215a6a2dd06f8157_80629159',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="table-responsive container">
<div class="page-header">
	<br/>
    <h1>Lista kont</h1>
</div>
<?php if (isset($_smarty_tpl->tpl_vars['message']->value)) {?>
	<div class="alert alert-success" role="alert"><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</div>
<?php }?>      
<?php if (isset($_smarty_tpl->tpl_vars['error']->value)) {?>
	<div class="alert alert-danger" role="alert"><?php echo $_smarty_tpl->tpl_vars['error']->value;?>
</div>
<?php }?>
	<?php if (count($_smarty_tpl->tpl_vars['accounts']->value) === 0) {?>
    <div class="alert alert-info" role="alert">Brak rekordów w bazie!</div>
<?php } else { ?> 
	<table id="data" class="table table-condensed table-striped" cellspacing="0" width="100%"> 
		<thead><tr>
             <th>id</th>
             <th>login</th> 
			 <th>hasło</th>			 
             <th>operacje</th> 
        </tr></thead>
		<tbody>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['accounts']->value, 'account', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['account']->value) {
?>
        <tr> 
		<td><?php echo $_smarty_tpl->tpl_vars['account']->value['idAccount'];?>
</td>
        <td><?php echo $_smarty_tpl->tpl_vars['account']->value['login'];?>
</td>
		<td><?php echo $_smarty_tpl->tpl_vars['account']->value['haslo'];?>
</td>		
        <td>
		<a type="button" class="btn btn-warning btn-xs" href="http://<?php echo $_SERVER['HTTP_HOST'];
echo $_smarty_tpl->tpl_vars['subdir']->value;?>
account/editform/<?php echo $_smarty_tpl->tpl_vars['account']->value['idAccount'];?>
">edytuj</a>&nbsp;&nbsp;
        <a type="button" class="btn btn-danger btn-xs" href="http://<?php echo $_SERVER['HTTP_HOST'];
echo $_smarty_tpl->tpl_vars['subdir']->value;?>
account/delete/<?php echo $_smarty_tpl->tpl_vars['account']->value['idAccount'];?>
">usuń</a>
		</td>
    </tr>
	<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

</tbody>
</table>
<?php }?>
		<br/>
		
</div>
<?php
}
}
/* {/block 'body'} */
}
