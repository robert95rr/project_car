<?php
	namespace Controllers;
	
    class Account extends Controller {
	
		public function getAll(){
			$view = $this->getView('Account');
            $data = null;
			if(\Tools\Session::is('message'))
                $data['message'] = \Tools\Session::get('message');
            if(\Tools\Session::is('error'))
                $data['error'] = \Tools\Session::get('error');
            $view->getAll($data);
           \Tools\Session::clear('message');
            \Tools\Session::clear('error');
		}
        public function addform(){                    
            $view = $this->getView('Account');
			$view->addform();
        }
        public function add(){                        
            $model=$this->getModel('Account');
            $data = $model->add($_POST['login'], $_POST['haslo']);
            //if(isset($data['error']))
                //\Tools\Session::set('error', $data['error']);
            //if(isset($data['message']))
                //\Tools\Session::set('message', $data['message']);
            $this->redirect('account');
        }
        public function delete($idAccount){ 
			$accessController = new \Controllers\Access();
            $accessController->islogin();
			
            $model=$this->getModel('Account'); 
            $data = $model->delete($idAccount);
            if(isset($data['error']))
                \Tools\Session::set('error', $data['error']);
            if(isset($data['message']))
                \Tools\Session::set('message', $data['message']);
			$this->redirect('account');            
        }
        public function editform($idAccount){
			$accessController = new \Controllers\Access();
            $accessController->islogin();
			
            $model = $this->getModel('Account');
            $data = $model->getOne($idAccount);
            if(isset($data['error'])){
                \Tools\Session::set('error', $data['error']);
                $this->redirect('account');
            }
            $view = $this->getView('Account');
			$view->editform($data['accounts'][0]);   
        }
        public function update(){
			$accessController = new \Controllers\Access();
            $accessController->islogin();
			
            $model=$this->getModel('Account');
            $data = $model->update($_POST['idAccount'], $_POST['login'], $_POST['haslo']);
            if(isset($data['error']))
                \Tools\Session::set('error', $data['error']);
            if(isset($data['message']))
                \Tools\Session::set('message', $data['message']);
                
            $this->redirect('account');
        }
            
			
	}
