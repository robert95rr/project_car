<?php
	namespace Controllers;

class Car extends Controller {
    
		public function getAll(){
			$view = $this->getView('Car');
            $data = null;
			if(\Tools\Session::is('message'))
                $data['message'] = \Tools\Session::get('message');
            if(\Tools\Session::is('error'))
                $data['error'] = \Tools\Session::get('error');
            $view->getAll($data);
			\Tools\Session::clear('message');
            \Tools\Session::clear('error');
            
		}
		public function ownerIs($idOwner){	
			$view = $this->getView('Car');
            $view->getAllInOwner($idOwner);
		}    
		public function inType($idBodyType){
			$view = $this->getView('Car');
			$view->getAllInBodyType($idBodyType);
		}
        public function addform(){
			$accessController = new \Controllers\Access();
            $accessController->islogin();
            $view = $this->getView('Car');
			$view->addform();
        }    
        public function add(){
			$accessController = new \Controllers\Access();
            $accessController->islogin();
			
            $model=$this->getModel('Car');
            $data = $model->add($_POST['brand'], $_POST['model'], $_POST['yearProduction'], $_POST['engineSize'], $_POST['enginePower'], $_POST['registrationNumber'], $_POST['vinNumber'], $_POST['fuel'], $_POST['colour'], $_POST['owner'], $_POST['bodytype']);
            if(isset($data['error']))
                \Tools\Session::set('error', $data['error']);
            if(isset($data['message']))
                \Tools\Session::set('message', $data['message']);
            $this->redirect('car');
        }
        public function delete($idCar){
			$accessController = new \Controllers\Access();
            $accessController->islogin();
			
            $model=$this->getModel('Car'); 
            $data = $model->delete($idCar);
            if(isset($data['error']))
                \Tools\Session::set('error', $data['error']);
            if(isset($data['message']))
                \Tools\Session::set('message', $data['message']);
			$this->redirect('car');            
        }  
    
        public function editform($idCar){ 
		    $accessController = new \Controllers\Access();
            $accessController->islogin(); 

            $model = $this->getModel('Car');
            $data = $model->getOne($idCar);
            if(isset($data['error'])){
                \Tools\Session::set('error', $data['error']);
                $this->redirect('car');
            }
            $view = $this->getView('Car');
			$view->editform($data['cars'][0]);   
        }
        public function update(){
			$accessController = new \Controllers\Access();
            $accessController->islogin(); 
			
            $model=$this->getModel('Car');
            $data = $model->update($_POST['idCar'], $_POST['brand'], $_POST['model'], $_POST['yearProduction'], $_POST['engineSize'], $_POST['enginePower'], $_POST['registrationNumber'], $_POST['vinNumber'], $_POST['fuel'], $_POST['colour'], $_POST['owner'], $_POST['bodytype']);
            if(isset($data['error']))
                \Tools\Session::set('error', $data['error']);
            $this->redirect('car');
        }    
        
	}
