<?php
	namespace Controllers;

	//kontroler do obsługi logowania i rejestracji!!!
	class Access extends Controller {

		//wyświetla formularz logowania
		public function logform($data = null) {
            if(\Tools\Access::islogin() === true)
                $this->redirect('');

            if(\Tools\Session::is('message'))
                $data['message'] = \Tools\Session::get('message');
            if(\Tools\Session::is('error'))
                $data['error'] = \Tools\Session::get('error');

			$view=$this->getView('Access');
			$view->logform($data);

            \Tools\Session::clear('message');
            \Tools\Session::clear('error');
		}

		//loguje do systemu
		public function login() {
            if(\Tools\Access::islogin() === true)
                $this->redirect('');

			$model=$this->getModel('Access');
			$data = $model->login($_POST['login'], $_POST['haslo']);

			if(!isset($data['error'])) {
				\Tools\Session::set('message', \Config\Website\MessageName::$login);
				$this->redirect('');
			} else
				$this->logform($data);
		}

		//wyświetla formularz rejestracji
		public function signupform($data = null) {
            if(\Tools\Access::islogin() === true)
                $this->redirect('');

            if(\Tools\Session::is('message'))
                $data['message'] = \Tools\Session::get('message');
            if(\Tools\Session::is('error'))
                $data['error'] = \Tools\Session::get('error');

			$view=$this->getView('Access');
			$view->signupform($data);

            \Tools\Session::clear('message');
            \Tools\Session::clear('error');
        }

		//rejestruje nowego użytkownika
		public function signup() {
            if(\Tools\Access::islogin() === true)
                $this->redirect('');

            $model=$this->getModel('Access');
			$data = $model->signup($_POST['login'], $_POST['haslo']);

			if(!isset($data['error'])) {
				\Tools\Session::set('message', \Config\Website\MessageName::$signup);
				$this->redirect('');
			} else
				$this->signupform($data);
        }

		//wylogowuje z systemu
		public function logout() {
			if(\Tools\Access::islogin() !== true)
                $this->redirect('');

			\Tools\Access::logout();
			\Tools\Session::set('message', \Config\Website\MessageName::$logout);
			$this->redirect('');
		}

		//sprawdza czy użytkownik jest zalogowany i zwraca jego id, jeśli nie jest wyświetla formularz logowania
        public function islogin() {
            if(\Tools\Access::islogin() !== true) {
                \Tools\Session::set('message', \Config\Website\MessageName::$mustlogin);
                $this->redirect('access/logform');
            }
            return \Tools\Access::getuserid();
        }
	}
