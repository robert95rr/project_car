<?php
	namespace Controllers;
	
    class Owner extends Controller {
	
		public function getAll(){
			$view = $this->getView('Owner');
            $data = null;
			if(\Tools\Session::is('message'))
                $data['message'] = \Tools\Session::get('message');
            if(\Tools\Session::is('error'))
                $data['error'] = \Tools\Session::get('error');
            $view->getAll($data);
			\Tools\Session::clear('message');
            \Tools\Session::clear('error');
           
		}
        public function addform(){ 
			$accessController = new \Controllers\Access();
            $accessController->islogin();
			
            $view = $this->getView('Owner');
			$view->addform();
        }
        public function add(){ 
			$accessController = new \Controllers\Access();
            $accessController->islogin();
			
            $model=$this->getModel('Owner');
            $data = $model->add($_POST['name'], $_POST['surname'], $_POST['phone']);
            if(isset($data['error']))
                \Tools\Session::set('error', $data['error']);
            if(isset($data['message']))
                \Tools\Session::set('message', $data['message']);
            $this->redirect('owner');
        }
        public function delete($idOwner){
			$accessController = new \Controllers\Access();
            $accessController->islogin();
			
            $model=$this->getModel('Owner'); 
            $data = $model->delete($idOwner);
            if(isset($data['error']))
                \Tools\Session::set('error', $data['error']);
            if(isset($data['message']))
                \Tools\Session::set('message', $data['message']);
			$this->redirect('owner');            
        }
        public function editform($idOwner){
			$accessController = new \Controllers\Access();
            $accessController->islogin();
			
            $model = $this->getModel('Owner');
            $data = $model->getOne($idOwner);
            if(isset($data['error'])){
                \Tools\Session::set('error', $data['error']);
                $this->redirect('owner');
            }
            $view = $this->getView('Owner');
			$view->editform($data['owners'][0]);   
        }
        public function update(){
			$accessController = new \Controllers\Access();
            $accessController->islogin();
			
            $model=$this->getModel('Owner');
            $data = $model->update($_POST['idOwner'], $_POST['name'], $_POST['surname'], $_POST['phone']);
            if(isset($data['error']))
                \Tools\Session::set('error', $data['error']);
            if(isset($data['message']))
                \Tools\Session::set('message', $data['message']);
            $this->redirect('owner');
        }
            
			
	}
