<?php
	namespace Controllers;

    class BodyType extends Controller {

		public function getAll(){
			$view = $this->getView('BodyType');
            $data = null;
			 if(\Tools\Session::is('message'))
                $data['message'] = \Tools\Session::get('message');
            if(\Tools\Session::is('error'))
                $data['error'] = \Tools\Session::get('error');
            $view->getAll($data);
           \Tools\Session::clear('message');
            \Tools\Session::clear('error');
		}
        public function addform(){
			$accessController = new \Controllers\Access();
            $accessController->islogin();

            $view = $this->getView('BodyType');
			$view->addform();
        }
        public function add(){
			$accessController = new \Controllers\Access();
            $accessController->islogin();

            $model=$this->getModel('BodyType');
            $data = $model->add($_POST['name']);
            if(isset($data['error']))
                \Tools\Session::set('error', $data['error']);
            if(isset($data['message']))
                \Tools\Session::set('message', $data['message']);
            //$this->redirect('bodytype');
        }
        public function delete($idBodyType){
			$accessController = new \Controllers\Access();
            $accessController->islogin();

            $model=$this->getModel('BodyType');
            $data = $model->delete($idBodyType);
            if(isset($data['error']))
                \Tools\Session::set('error', $data['error']);
            if(isset($data['message']))
                \Tools\Session::set('message', $data['message']);
			$this->redirect('bodytype');
        }
        public function editform($idBodyType){
			$accessController = new \Controllers\Access();
            $accessController->islogin();

            $model = $this->getModel('BodyType');
            $data = $model->getOne($idBodyType);
            if(isset($data['error'])){
                \Tools\Session::set('error', $data['error']);
                $this->redirect('bodytype');
            }
            $view = $this->getView('BodyType');
			$view->editform($data['bodytypes'][0]);
        }
        public function update(){
			$accessController = new \Controllers\Access();
            $accessController->islogin();

            $model=$this->getModel('BodyType');
            $data = $model->update($_POST['idBodyType'], $_POST['name']);
            if(isset($data['error']))
                \Tools\Session::set('error', $data['error']);
            if(isset($data['message']))
                \Tools\Session::set('message', $data['message']);
            $this->redirect('bodytype');
        }
		public function autocomplete($idBodyType = null){
			if($idBodyType != null) {
				$model=$this->getModel('BodyType');
            	$data = $model->autocomplete($idBodyType);

				if(!isset($data['error']))
					echo json_encode($data['bodytypes']);
				else
					echo "[]";
			}
			else
				echo "[]";
		}


	}
