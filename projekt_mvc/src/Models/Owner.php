<?php
	namespace Models;
	use \PDO;
	class Owner extends Model {

		public function getAll(){
            if($this->pdo === null){
                $data['error'] = \Config\Database\DBErrorName::$connection;
                return $data;
            }
            $data = array();
            $data['$owners'] = array();
            try	{
                $stmt = $this->pdo->query('SELECT * FROM `'.\Config\Database\DBConfig::$tableOwner.'`');
                $owners = $stmt->fetchAll();
                $stmt->closeCursor();
                if($owners && !empty($owners))
                    $data['owners'] = $owners;
            }
            catch(\PDOException $e)	{
                $data['error'] = \Config\Database\DBErrorName::$query;
            }	
            return $data;
		}        
       public function getAllForSelect(){
            $data = $this->getAll();
			$owners = array();            
            if(!isset($data['error']))                
            foreach($data['owners'] as $idOwner)                   
                $owners[$idOwner[\Config\Database\DBConfig\Owner::$idOwner]] = $idOwner[\Config\Database\DBConfig\Owner::$name];
            return $owners;            
        }
		public function getOne($idOwner){
            if($this->pdo === null){
                $data['error'] = \Config\Database\DBErrorName::$connection;
                return $data;
            }
            if($idOwner === null){
                $data['error'] = \Config\Database\DBErrorName::$nomatch;
                return $data;
            }          
            $data = array();
            $data['owners'] = array();
            try	{
                $stmt = $this->pdo->prepare('SELECT * FROM  `'.\Config\Database\DBConfig::$tableOwner.'` WHERE  `'.\Config\Database\DBConfig\Owner::$idOwner.'`=:idOwner');   
                $stmt->bindValue(':idOwner', $idOwner, PDO::PARAM_INT); 
                $result = $stmt->execute(); 
                $owners = $stmt->fetchAll();
                $stmt->closeCursor();
                if($owners && !empty($owners))
                    $data['owners'] = $owners;
                else
                    $data['error'] = \Config\Database\DBErrorName::$nomatch;
            }
            catch(\PDOException $e)	{
                var_dump($e);
                $data['error'] = \Config\Database\DBErrorName::$query;
            }	
            return $data;
		}        

		public function add($name, $surname, $phone){
            if($this->pdo === null){
                $data['error'] = \Config\Database\DBErrorName::$connection;
                return $data;
            }
            if($name === null){
                $data['error'] = \Config\Database\DBErrorName::$empty;
                return $data;
            }              
            $data = array();
            try	{
                $stmt = $this->pdo->prepare('INSERT INTO `'.\Config\Database\DBConfig::$tableOwner.'` (`'.\Config\Database\DBConfig\Owner::$name.'`, `'.\Config\Database\DBConfig\Owner::$surname.'`, `'.\Config\Database\DBConfig\Owner::$phone.'`) VALUES (:name, :surname, :phone)');                   
                $stmt->bindValue(':name', $name, PDO::PARAM_STR); 
				$stmt->bindValue(':surname', $surname, PDO::PARAM_STR);
				$stmt->bindValue(':phone', $phone, PDO::PARAM_STR);
                $result = $stmt->execute(); 
                if(!$result)
                    $data['error'] = \Config\Database\DBErrorName::$noadd;
                else
                    $data['message'] = \Config\Database\DBMessageName::$addok;
                $stmt->closeCursor();                 
            }
            catch(\PDOException $e)	{
                $data['error'] = \Config\Database\DBErrorName::$query;
            }	
            return $data;
		}     
		public function delete($idOwner){
            $data = array();
            if($this->pdo === null){
                $data['error'] = \Config\Database\DBErrorName::$connection;
                return $data;
            }
            if($idOwner === null){
                $data['error'] = \Config\Database\DBErrorName::$nomatch;
                return $data;
            }
            try	{
                $stmt = $this->pdo->prepare('DELETE FROM  `'.\Config\Database\DBConfig::$tableOwner.'` WHERE  `'.\Config\Database\DBConfig\Owner::$idOwner.'`=:idOwner');   
                $stmt->bindValue(':idOwner', $idOwner, PDO::PARAM_INT); 
                $result = $stmt->execute(); 
                if(!$result)
                    $data['error'] = \Config\Database\DBErrorName::$nomatch;
                else
                    $data['message'] = \Config\Database\DBMessageName::$deleteok;
                $stmt->closeCursor();                 
            }
            catch(\PDOException $e)	{
                $data['error'] = \Config\Database\DBErrorName::$query;
            }
            return $data;
		}  
        
		public function update($idOwner, $name, $surname, $phone){
            $data = array();
            if($this->pdo === null){
                $data['error'] = \Config\Database\DBErrorName::$connection;
                return $data;
            }
            if($idOwner === null || $name === null || $surname === null || $phone === null){
                $data['error'] = \Config\Database\DBErrorName::$empty;
                return $data;
            }
            try	{
                $stmt = $this->pdo->prepare('UPDATE  `'.\Config\Database\DBConfig::$tableOwner.'` SET
                `'.\Config\Database\DBConfig\Owner::$name.'`=:name,  `'
				.\Config\Database\DBConfig\Owner::$surname.'`=:surname,  `'
				.\Config\Database\DBConfig\Owner::$phone.'`=:phone WHERE `'
                .\Config\Database\DBConfig\Owner::$idOwner.'`=:idOwner');   
                $stmt->bindValue(':idOwner', $idOwner, PDO::PARAM_INT);
                $stmt->bindValue(':name', $name, PDO::PARAM_STR);
				$stmt->bindValue(':surname', $surname, PDO::PARAM_STR);
				$stmt->bindValue(':phone', $phone, PDO::PARAM_STR);
                
                $result = $stmt->execute(); 
                $rows = $stmt->rowCount();
                if(!$result)
                    $data['error'] = \Config\Database\DBErrorName::$nomatch;
                else
                    $data['message'] = \Config\Database\DBMessageName::$updateok;
                $stmt->closeCursor();                 
            }
            catch(\PDOException $e)	{
                $data['error'] = \Config\Database\DBErrorName::$query;
            }
            return $data;
		}         
	}

