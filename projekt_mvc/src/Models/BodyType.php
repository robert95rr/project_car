<?php
	namespace Models;
	use \PDO;
	class BodyType extends Model {

		public function getAll(){
            if($this->pdo === null){
                $data['error'] = \Config\Database\DBErrorName::$connection;
                return $data;
            }
            $data = array();
            $data['$bodytypes'] = array();
            try	{
                $stmt = $this->pdo->query('SELECT * FROM `'.\Config\Database\DBConfig::$tableBodyType.'`');
                $bodytypes = $stmt->fetchAll();
                $stmt->closeCursor();
                if($bodytypes && !empty($bodytypes))
                    $data['bodytypes'] = $bodytypes;
            }
            catch(\PDOException $e)	{
                $data['error'] = \Config\Database\DBErrorName::$query;
            }
            return $data;
		}
        public function getAllForSelect(){
            $data = $this->getAll();
			$bodytypes = array();
            if(!isset($data['error']))
            foreach($data['bodytypes'] as $idBodyType)
                $bodytypes[$idBodyType[\Config\Database\DBConfig\BodyType::$idBodyType]] = $idBodyType[\Config\Database\DBConfig\BodyType::$name];
            return $bodytypes;
        }
		public function getOne($idBodyType){
            if($this->pdo === null){
                $data['error'] = \Config\Database\DBErrorName::$connection;
                return $data;
            }
            if($idBodyType === null){
                $data['error'] = \Config\Database\DBErrorName::$nomatch;
                return $data;
            }
            $data = array();
            $data['bodytypes'] = array();
            try	{
                $stmt = $this->pdo->prepare('SELECT * FROM  `'.\Config\Database\DBConfig::$tableBodyType.'` WHERE  `'.\Config\Database\DBConfig\BodyType::$idBodyType.'`=:idBodyType');
                $stmt->bindValue(':idBodyType', $idBodyType, PDO::PARAM_INT);
                $result = $stmt->execute();
                $bodytypes = $stmt->fetchAll();
                $stmt->closeCursor();
                if($bodytypes && !empty($bodytypes))
                    $data['bodytypes'] = $bodytypes;
                else
                    $data['error'] = \Config\Database\DBErrorName::$nomatch;
            }
            catch(\PDOException $e)	{
                var_dump($e);
                $data['error'] = \Config\Database\DBErrorName::$query;
            }
            return $data;
		}

		public function add($name){
            if($this->pdo === null){
                $data['error'] = \Config\Database\DBErrorName::$connection;
                return $data;
            }
            if($name === null){
                $data['error'] = \Config\Database\DBErrorName::$empty;
                return $data;
            }
            $data = array();
            try	{
                $stmt = $this->pdo->prepare('INSERT INTO `'.\Config\Database\DBConfig::$tableBodyType.'` (`'.\Config\Database\DBConfig\BodyType::$name.'`) VALUES (:name)');

                $stmt->bindValue(':name', $name, PDO::PARAM_STR);
                $result = $stmt->execute();
                if(!$result)
                    $data['error'] = \Config\Database\DBErrorName::$noadd;
                else
                    $data['message'] = \Config\Database\DBMessageName::$addok;
                $stmt->closeCursor();
            }
            catch(\PDOException $e)	{
                $data['error'] = \Config\Database\DBErrorName::$query;
            }

						//AJAX - pobranie wszystkich danych z tabeli bodytype
						$sth = $this->pdo->prepare('SELECT * FROM `'.\Config\Database\DBConfig::$tableBodyType.'`');
						$sth->execute();
						$result = $sth->fetchAll();
            echo json_encode($result);
		}
		public function delete($idBodyType){
            $data = array();
            if($this->pdo === null){
                $data['error'] = \Config\Database\DBErrorName::$connection;
                return $data;
            }
            if($idBodyType === null){
                $data['error'] = \Config\Database\DBErrorName::$nomatch;
                return $data;
            }
            try	{
                $stmt = $this->pdo->prepare('DELETE FROM  `'.\Config\Database\DBConfig::$tableBodyType.'` WHERE  `'.\Config\Database\DBConfig\BodyType::$idBodyType.'`=:idBodyType');
                $stmt->bindValue(':idBodyType', $idBodyType, PDO::PARAM_INT);
                $result = $stmt->execute();
                if(!$result)
                    $data['error'] = \Config\Database\DBErrorName::$nomatch;
                else
                    $data['message'] = \Config\Database\DBMessageName::$deleteok;
                $stmt->closeCursor();
            }
            catch(\PDOException $e)	{
                $data['error'] = \Config\Database\DBErrorName::$query;
            }
            return $data;
		}

		public function update($idBodyType, $name){
            $data = array();
            if($this->pdo === null){
                $data['error'] = \Config\Database\DBErrorName::$connection;
                return $data;
            }
            if($idBodyType === null || $name === null){
                $data['error'] = \Config\Database\DBErrorName::$empty;
                return $data;
            }
            try	{
                $stmt = $this->pdo->prepare('UPDATE  `'.\Config\Database\DBConfig::$tableBodyType.'` SET
                `'.\Config\Database\DBConfig\BodyType::$name.'`=:name WHERE `'
                .\Config\Database\DBConfig\BodyType::$idBodyType.'`=:idBodyType');
                $stmt->bindValue(':idBodyType', $idBodyType, PDO::PARAM_INT);
                $stmt->bindValue(':name', $name, PDO::PARAM_STR);

                $result = $stmt->execute();
                $rows = $stmt->rowCount();


                if(!$result)
                    $data['error'] = \Config\Database\DBErrorName::$nomatch;
                else
                    $data['message'] = \Config\Database\DBMessageName::$updateok;
                $stmt->closeCursor();
            }
            catch(\PDOException $e)	{
                $data['error'] = \Config\Database\DBErrorName::$query;
            }
            return $data;
		}

	   public function autocomplete($term) {
			$data = $this->getAll();

		if(!isset($data['error']))
			$data['bodytypes'] = array_filter($data['bodytypes'],
				function ($var) {
					 return (stripos($var['name'], $_GET['id']) !== false);
				}
			);
		return $data;
	}
	}
