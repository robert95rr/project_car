<?php
	namespace Models;
	use \PDO;
	class Account extends Model {

		public function getAll(){
            if($this->pdo === null){
                $data['error'] = \Config\Database\DBErrorName::$connection;
                return $data;
            }
            $data = array();
            $data['$accounts'] = array();
            try	{
                $stmt = $this->pdo->query('SELECT * FROM `'.\Config\Database\DBConfig::$tableAccount.'`');
                $accounts = $stmt->fetchAll();
                $stmt->closeCursor();
                if($accounts && !empty($accounts))
                    $data['accounts'] = $accounts;
            }
            catch(\PDOException $e)	{
                $data['error'] = \Config\Database\DBErrorName::$query;
            }	
            return $data;
		}        
       /* public function getAllForSelect(){
            $data = $this->getAll();
			$categories = array();            
            if(!isset($data['error']))                
            foreach($data['categories'] as $category)                   
                $categories[$category[\Config\Database\DBConfig\Category::$id]] = $category[\Config\Database\DBConfig\Category::$name];
            return $categories;            
        }*/
		public function getOne($idAccount){
            if($this->pdo === null){
                $data['error'] = \Config\Database\DBErrorName::$connection;
                return $data;
            }
            if($idAccount === null){
                $data['error'] = \Config\Database\DBErrorName::$nomatch;
                return $data;
            }          
            $data = array();
            $data['accounts'] = array();
            try	{
                $stmt = $this->pdo->prepare('SELECT * FROM  `'.\Config\Database\DBConfig::$tableAccount.'` WHERE  `'.\Config\Database\DBConfig\Account::$idAccount.'`=:idAccount');   
                $stmt->bindValue(':idAccount', $idAccount, PDO::PARAM_INT); 
                $result = $stmt->execute(); 
                $accounts = $stmt->fetchAll();
                $stmt->closeCursor();
                if($accounts && !empty($accounts))
                    $data['accounts'] = $accounts;
                else
                    $data['error'] = \Config\Database\DBErrorName::$nomatch;
            }
            catch(\PDOException $e)	{
                var_dump($e);
                $data['error'] = \Config\Database\DBErrorName::$query;
            }	
            return $data;
		}        

		public function add($login, $haslo){
            if($this->pdo === null){
                $data['error'] = \Config\Database\DBErrorName::$connection;
                return $data;
            }
            if($login === null){
                $data['error'] = \Config\Database\DBErrorName::$empty;
                return $data;
            }              
            $data = array();
            try	{
                $stmt = $this->pdo->prepare('INSERT INTO `'.\Config\Database\DBConfig::$tableAccount.'` (`'.\Config\Database\DBConfig\Account::$login.'`, `'.\Config\Database\DBConfig\Account::$haslo.'`) VALUES (:login, :haslo)');                   
                $stmt->bindValue(':login', $login, PDO::PARAM_STR); 
				$stmt->bindValue(':haslo', md5($haslo), PDO::PARAM_STR);
                $result = $stmt->execute(); 
                if(!$result)
                    $data['error'] = \Config\Database\DBErrorName::$noadd;
                else
                    $data['message'] = \Config\Database\DBMessageName::$addok;
                $stmt->closeCursor();                 
            }
            catch(\PDOException $e)	{
                $data['error'] = \Config\Database\DBErrorName::$query;
            }	
            return $data;
		}     
		public function delete($idAccount){
            $data = array();
            if($this->pdo === null){
                $data['error'] = \Config\Database\DBErrorName::$connection;
                return $data;
            }
            if($idAccount === null){
                $data['error'] = \Config\Database\DBErrorName::$nomatch;
                return $data;
            }
            try	{
                $stmt = $this->pdo->prepare('DELETE FROM  `'.\Config\Database\DBConfig::$tableAccount.'` WHERE  `'.\Config\Database\DBConfig\Account::$idAccount.'`=:idAccount');   
                $stmt->bindValue(':idAccount', $idAccount, PDO::PARAM_INT); 
                $result = $stmt->execute(); 
                if(!$result)
                    $data['error'] = \Config\Database\DBErrorName::$nomatch;
                else
                    $data['message'] = \Config\Database\DBMessageName::$deleteok;
                $stmt->closeCursor();                 
            }
            catch(\PDOException $e)	{
                $data['error'] = \Config\Database\DBErrorName::$query;
            }
            return $data;
		}  
        
		public function update($idAccount, $login, $haslo){
             
            

            $data = array();
            if($this->pdo === null){
                $data['error'] = \Config\Database\DBErrorName::$connection;
                return $data;
            }
            if($idAccount === null || $login === null || $haslo === null){
                $data['error'] = \Config\Database\DBErrorName::$empty;
                return $data;
            }
            try	{
                $stmt = $this->pdo->prepare('UPDATE  `'.\Config\Database\DBConfig::$tableAccount.'` SET
                `'.\Config\Database\DBConfig\Account::$login.'`=:login,  `'
				.\Config\Database\DBConfig\Account::$haslo.'`=:haslo WHERE `'
                .\Config\Database\DBConfig\Account::$idAccount.'`=:idAccount');   
                $stmt->bindValue(':idAccount', $idAccount, PDO::PARAM_INT);
                $stmt->bindValue(':login', $login, PDO::PARAM_STR);
				$stmt->bindValue(':haslo', $haslo, PDO::PARAM_STR);
                
                $result = $stmt->execute(); 
                $rows = $stmt->rowCount();
                
                if(!$result)
                    $data['error'] = \Config\Database\DBErrorName::$nomatch;
                else
                    $data['message'] = \Config\Database\DBMessageName::$updateok;
                $stmt->closeCursor();                 
            }
            catch(\PDOException $e)	{
                //echo $e;
                $data['error'] = \Config\Database\DBErrorName::$query;
            }
            return $data;
		}         
	}
