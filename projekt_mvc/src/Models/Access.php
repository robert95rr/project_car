<?php
namespace Models;
use \PDO;

class Access extends Model {
	
    public function login($login, $haslo) {
        if($this->pdo == null) {
            $data['error'] = \Config\Database\DBErrorName::$connection;
            return $data;
        }

        if($login == null || $haslo == null) {
            $data['error'] = \Config\Database\DBErrorName::$nomatch;
            return $data;
        }

        $data = array();
        try {
            $stmt = $this->pdo->prepare(
				'SELECT * FROM `'.\Config\Database\DBConfig::$tableAccount.'`
				WHERE `'.\Config\Database\DBConfig\Account::$login.'`=:login');

            $stmt->bindValue(':login', $login, PDO::PARAM_STR);
            $result = $stmt->execute();
            $account = $stmt->fetchAll();
            $stmt->closeCursor();

            if($account && !empty($account)) {
                if($account[0][\Config\Database\DBConfig\Account::$haslo] == md5($haslo)) {//Tutaj jest coś nie tak!!!!!!!!!!!!!!!!!!!!!!!!
                    //zainicjalizowanie sesji
                    \Tools\Access::login($login, $account[0][\Config\Database\DBConfig\Account::$idAccount]);
                    return $data;
                } else
					echo 'Error!!!!!!!!!';exit();//
                    $data['error'] = \Config\Website\ErrorName::$wrongdata;
            } else
                $data['error'] = \Config\Website\ErrorName::$wrongdata;
        } catch(\PDOException $e) {
            $data['error'] = \Config\Database\DBErrorName::$query;
        }
        return $data;
    }
//ZMIANA!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    public function signup($login, $haslo) {
		if($this->pdo == null) {
			$data['error'] = \Config\Database\DBErrorName::$connection;
			return $data;
		}

		if($login == null) {
			$data['error'] = \Config\Database\DBErrorName::$empty;
			return $data;
		}

		try {
			$stmt = $this->pdo->prepare(
				'INSERT INTO `'.\Config\Database\DBConfig::$tableAccount.'` (`'.\Config\Database\DBConfig\Account::$login.'`,`'.\Config\Database\DBConfig\Account::$haslo.'`) VALUES (:login, :haslo)');

			$stmt->bindValue(':login', $login, PDO::PARAM_STR);
			$stmt->bindValue(':haslo', md5($haslo), PDO::PARAM_STR);

			$result = $stmt->execute();
			$stmt->closeCursor();

			if($result)
				$data['message'] = \Config\Database\DBMessageName::$addok;
			else
				$data['error'] = \Config\Database\DBErrorName::$noadd;
		} catch(\PDOException $e) {
			$data['error'] = \Config\Database\DBErrorName::$query;
		}
		return $data;
    }
}
