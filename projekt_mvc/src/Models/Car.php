<?php
	namespace Models;
	use \PDO;
	class Car extends Model {
	
		public function getAll($idOwner = null, $idBodyType = null){
            if($this->pdo === null){
                $data['error'] = \Config\Database\DBErrorName::$connection;
                return $data;
            }
            $data = array();
            $data['cars'] = array();
            try	{
				
                $query = 'SELECT `';
                $query .= \Config\Database\DBConfig::$tableCar.'`.*, `';
                $query .= \Config\Database\DBConfig::$tableBodyType.'`.'.\Config\Database\DBConfig\BodyType::$name.' AS bodytype_name, `'; 
                $query .= \Config\Database\DBConfig::$tableOwner.'`.'.\Config\Database\DBConfig\Owner::$name.' AS owner_name';
				$query .= ' FROM `';
                $query .= \Config\Database\DBConfig::$tableCar.'`, `';
                $query .= \Config\Database\DBConfig::$tableBodyType.'`, `';
				$query .= \Config\Database\DBConfig::$tableOwner.'`';
                $query .= ' WHERE `';
                $query .= \Config\Database\DBConfig::$tableCar.'`.'.\Config\Database\DBConfig\Car::$idOwner.' = `'.\Config\Database\DBConfig::$tableOwner.'`.'.\Config\Database\DBConfig\Owner::$idOwner;
                $query .= ' AND `';
				$query .= \Config\Database\DBConfig::$tableCar.'`.'.\Config\Database\DBConfig\Car::$idBodyType.' = `'.\Config\Database\DBConfig::$tableBodyType.'`.'.\Config\Database\DBConfig\BodyType::$idBodyType;
                $query .= ' ORDER BY`';
				$query .= \Config\Database\DBConfig\Car::$idCar.'`ASC';
				
				if($idOwner !== null){
					$query .= ' AND `';
                    $query .= \Config\Database\DBConfig::$tableCar.'`.'.\Config\Database\DBConfig\Car::$idOwner.' = :id_owner';
					$stmt = $this->pdo->prepare($query);
                    $stmt->bindValue(':id_owner', $idOwner, PDO::PARAM_INT);
					$result = $stmt->execute();
				}else if($idBodyType !== null){
					$query .= ' AND `';
                    $query .= \Config\Database\DBConfig::$tableCar.'`.'.\Config\Database\DBConfig\Car::$idBodyType.' = :id_bodytype';
					$stmt = $this->pdo->prepare($query);
                    $stmt->bindValue(':id_bodytype', $idBodyType, PDO::PARAM_INT);
					$result = $stmt->execute();
				}else{
					$stmt = $this->pdo->query($query);
				}
                $cars = $stmt->fetchAll();
                $stmt->closeCursor();
                if($cars && !empty($cars))
                {
                    $data['cars'] = $cars;
                }                                  
            }
            catch(\PDOException $e)	{
                $data['error'] = \Config\Database\DBErrorName::$query;
            }	
            return $data;
		}  
        
		public function getOne($idCar){
            if($this->pdo === null){
                $data['error'] = \Config\Database\DBErrorName::$connection;
                return $data;
            }
            if($idCar === null){
                $data['error'] = \Config\Database\DBErrorName::$nomatch;
                return $data;
            }          
            $data = array();
            $data['cars'] = array();
            try	{
				$query = 'SELECT `';
                $query .= \Config\Database\DBConfig::$tableCar.'`.* ,`';
                $query .= \Config\Database\DBConfig::$tableBodyType.'`.'.\Config\Database\DBConfig\BodyType::$name.' AS bodytype_name, `'; 
                $query .= \Config\Database\DBConfig::$tableOwner.'`.'.\Config\Database\DBConfig\Owner::$name.' AS owner_name';
				$query .= ' FROM `';
                $query .= \Config\Database\DBConfig::$tableCar.'`, `';
                $query .= \Config\Database\DBConfig::$tableBodyType.'`, `';
				$query .= \Config\Database\DBConfig::$tableOwner.'` ';
                $query .= ' WHERE `';
                $query .= \Config\Database\DBConfig::$tableCar.'`.'.\Config\Database\DBConfig\Car::$idOwner.' = `'.\Config\Database\DBConfig::$tableOwner.'`.'.\Config\Database\DBConfig\Owner::$idOwner;
                $query .= ' AND `';
				$query .= \Config\Database\DBConfig::$tableCar.'`.'.\Config\Database\DBConfig\Car::$idBodyType.'=`'.\Config\Database\DBConfig::$tableBodyType.'`.'.\Config\Database\DBConfig\BodyType::$idBodyType;       
                $query .= ' AND `';
                $query .= \Config\Database\DBConfig::$tableCar.'`.'.\Config\Database\DBConfig\Car::$idCar.'=:idCar';
                //echo $query;exit();
                $stmt = $this->pdo->prepare($query); 
                $stmt->bindValue(':idCar', $idCar, PDO::PARAM_INT);
				//$stmt->bindValue(':idOwner', $idOwner, PDO::PARAM_INT);				
                $result = $stmt->execute(); 
                $cars = $stmt->fetchAll();
                $stmt->closeCursor();
                if($cars && !empty($cars))
                    $data['cars'] = $cars;
                else
                    $data['error'] = \Config\Database\DBErrorName::$nomatch;
            }
            catch(\PDOException $e)	{
                //var_dump($e);
                $data['error'] = \Config\Database\DBErrorName::$query;
            }
            return $data;
		}  

//---------------------------------------------------------------------------CarInBodyType-----------------------------------------------------------------------------------------------------------------
		public function getBodyTypeinCar($idBodyType){
            if($this->pdo === null){
                $data['error'] = \Config\Database\DBErrorName::$connection;
                return $data;
            }
            if($idBodyType === null){
                $data['error'] = \Config\Database\DBErrorName::$nomatch;
                return $data;
            }          
            $data = array();
            $data['cars'] = array();
            try	{
				$query = 'SELECT `';
                $query .= \Config\Database\DBConfig::$tableCar.'`.* ,`';
                $query .= \Config\Database\DBConfig::$tableBodyType.'`.'.\Config\Database\DBConfig\BodyType::$name.' AS bodytype_name, `'; 
                $query .= \Config\Database\DBConfig::$tableOwner.'`.'.\Config\Database\DBConfig\Owner::$name.' AS owner_name';
				$query .= ' FROM `';
                $query .= \Config\Database\DBConfig::$tableCar.'`, `';
                $query .= \Config\Database\DBConfig::$tableBodyType.'`, `';
				$query .= \Config\Database\DBConfig::$tableOwner.'` ';
                $query .= ' WHERE `';
                $query .= \Config\Database\DBConfig::$tableCar.'`.'.\Config\Database\DBConfig\Car::$idOwner.' = `'.\Config\Database\DBConfig::$tableOwner.'`.'.\Config\Database\DBConfig\Owner::$idOwner;
                $query .= ' AND `';
				$query .= \Config\Database\DBConfig::$tableCar.'`.'.\Config\Database\DBConfig\Car::$idBodyType.'=`'.\Config\Database\DBConfig::$tableBodyType.'`.'.\Config\Database\DBConfig\BodyType::$idBodyType;       
                $query .= ' AND `';
                $query .= \Config\Database\DBConfig::$tableCar.'`.'.\Config\Database\DBConfig\Car::$idBodyType.'=:idBodyType';
                //echo $query;exit();
                $stmt = $this->pdo->prepare($query); 
                $stmt->bindValue(':idBodyType', $idBodyType, PDO::PARAM_INT);
				//$stmt->bindValue(':idOwner', $idOwner, PDO::PARAM_INT);				
                $result = $stmt->execute(); 
                $cars = $stmt->fetchAll();
                $stmt->closeCursor();
                if($cars && !empty($cars))
                    $data['cars'] = $cars;
                else
                    $data['error'] = \Config\Database\DBErrorName::$nomatch;
            }
            catch(\PDOException $e)	{
                //var_dump($e);
                $data['error'] = \Config\Database\DBErrorName::$query;
            }
            return $data;
		} 
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------		
//-----------------------------------------------------------------------------CarInOwner-----------------------------------------------------------------------------------------------------------------------        
		public function getOwnerInCar($idOwner){
            if($this->pdo === null){
                $data['error'] = \Config\Database\DBErrorName::$connection;
                return $data;
            }
            if($idOwner === null){
                $data['error'] = \Config\Database\DBErrorName::$nomatch;
                return $data;
            }          
            $data = array();
            $data['cars'] = array();
            try	{
				$query = 'SELECT `';
                $query .= \Config\Database\DBConfig::$tableCar.'`.* ,`';
                $query .= \Config\Database\DBConfig::$tableBodyType.'`.'.\Config\Database\DBConfig\BodyType::$name.' AS bodytype_name, `'; 
                $query .= \Config\Database\DBConfig::$tableOwner.'`.'.\Config\Database\DBConfig\Owner::$name.' AS owner_name';
				$query .= ' FROM `';
                $query .= \Config\Database\DBConfig::$tableCar.'`, `';
                $query .= \Config\Database\DBConfig::$tableBodyType.'`, `';
				$query .= \Config\Database\DBConfig::$tableOwner.'` ';
                $query .= ' WHERE `';
                $query .= \Config\Database\DBConfig::$tableCar.'`.'.\Config\Database\DBConfig\Car::$idOwner.' = `'.\Config\Database\DBConfig::$tableOwner.'`.'.\Config\Database\DBConfig\Owner::$idOwner;
                $query .= ' AND `';
				$query .= \Config\Database\DBConfig::$tableCar.'`.'.\Config\Database\DBConfig\Car::$idBodyType.'=`'.\Config\Database\DBConfig::$tableBodyType.'`.'.\Config\Database\DBConfig\BodyType::$idBodyType;       
                $query .= ' AND `';
                $query .= \Config\Database\DBConfig::$tableCar.'`.'.\Config\Database\DBConfig\Car::$idOwner.'=:idOwner';
                //echo $query;exit();
                $stmt = $this->pdo->prepare($query); 
                $stmt->bindValue(':idOwner', $idOwner, PDO::PARAM_INT);
				//$stmt->bindValue(':idOwner', $idOwner, PDO::PARAM_INT);				
                $result = $stmt->execute(); 
                $cars = $stmt->fetchAll();
                $stmt->closeCursor();
                if($cars && !empty($cars))
                    $data['cars'] = $cars;
                else
                    $data['error'] = \Config\Database\DBErrorName::$nomatch;
            }
            catch(\PDOException $e)	{
                //var_dump($e);
                $data['error'] = \Config\Database\DBErrorName::$query;
            }
            return $data;
		}  
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		public function add($brand, $model, $yearProduction, $engineSize, $enginePower, $registrationNumber, $vinNumber, $fuel, $colour, $idOwner, $idBodyType){
            if($this->pdo === null){
                $data['error'] = \Config\Database\DBErrorName::$connection;
                return $data;
            }
            if($brand === null || $model === null || $yearProduction === null || $engineSize === null || $registrationNumber == null || $vinNumber == null || $fuel == null || $colour == null || $idOwner == null || $idBodyType == null){
                $data['error'] = \Config\Database\DBErrorName::$empty;
                return $data;
            }
            $data = array();
            try	{
                $stmt = $this->pdo->prepare('INSERT INTO `'.\Config\Database\DBConfig::$tableCar.'` (`'.\Config\Database\DBConfig\Car::$brand.'`,`'.\Config\Database\DBConfig\Car::$model.'`, `'.\Config\Database\DBConfig\Car::$yearProduction.'`,`'.\Config\Database\DBConfig\Car::$engineSize.'`,`'.\Config\Database\DBConfig\Car::$enginePower.'`,`'.\Config\Database\DBConfig\Car::$registrationNumber.'`,`'.\Config\Database\DBConfig\Car::$vinNumber.'`,`'.\Config\Database\DBConfig\Car::$fuel.'`,`'.\Config\Database\DBConfig\Car::$colour.'`,`'.\Config\Database\DBConfig\Car::$idOwner.'`,`'.\Config\Database\DBConfig\Car::$idBodyType.'`) VALUES (:brand, :model, :yearProduction, :engineSize, :enginePower, :registrationNumber, :vinNumber, :fuel, :colour, :idOwner, :idBodyType)');
				$stmt->bindValue(':brand', $brand, PDO::PARAM_STR);
                $stmt->bindValue(':model', $model, PDO::PARAM_STR); 
                $stmt->bindValue(':yearProduction', $yearProduction, PDO::PARAM_STR);
                $stmt->bindValue(':engineSize', $engineSize, PDO::PARAM_STR);
				$stmt->bindValue(':enginePower', $enginePower, PDO::PARAM_STR);
				$stmt->bindValue(':registrationNumber', $registrationNumber, PDO::PARAM_STR);
				$stmt->bindValue(':vinNumber', $vinNumber, PDO::PARAM_STR);
				$stmt->bindValue(':fuel', $fuel, PDO::PARAM_STR);
				$stmt->bindValue(':colour', $colour, PDO::PARAM_STR);
				$stmt->bindValue(':idOwner', $idOwner, PDO::PARAM_INT);
				$stmt->bindValue(':idBodyType', $idBodyType, PDO::PARAM_INT);
                $result = $stmt->execute(); 
                if(!$result)
                    $data['error'] = \Config\Database\DBErrorName::$noadd;
                else
                    $data['message'] = \Config\Database\DBMessageName::$addok;
                $stmt->closeCursor();
            }
            catch(\PDOException $e)	{
                $data['error'] = \Config\Database\DBErrorName::$query;
            }
            return $data;
		}         
		public function delete($idCar){
            $data = array();
            if($this->pdo === null){
                $data['error'] = \Config\Database\DBErrorName::$connection;
                return $data;
            }
            if($idCar === null){
                $data['error'] = \Config\Database\DBErrorName::$nomatch;
                return $data;
            }
            try	{
                $stmt = $this->pdo->prepare('DELETE FROM  `'.\Config\Database\DBConfig::$tableCar.'` WHERE  `'.\Config\Database\DBConfig\Car::$idCar.'`=:idCar');    
                $stmt->bindValue(':idCar', $idCar, PDO::PARAM_INT); 
				//$stmt->bindValue(':idOwner', $idOwner, PDO::PARAM_INT); 
                $result = $stmt->execute(); 
                if(!$result)
                    $data['error'] = \Config\Database\DBErrorName::$nomatch;
                else
                    $data['message'] = \Config\Database\DBMessageName::$deleteok;
                $stmt->closeCursor();                 
            }
            catch(\PDOException $e)	{
                $data['error'] = \Config\Database\DBErrorName::$query;
            }
            return $data;
		} 
        
		public function update($idCar, $brand, $model, $yearProduction, $engineSize, $enginePower, $registrationNumber, $vinNumber, $fuel, $colour, $idOwner, $idBodyType){
            $data = array();
            if($this->pdo === null){
                $data['error'] = \Config\Database\DBErrorName::$connection;
                return $data;
            }
            if($idCar === null || $brand === null || $model === null || $yearProduction === null || $engineSize === null || $enginePower === null || $registrationNumber === null || $vinNumber === null || $fuel === null || $colour === null || $idOwner === null || $idBodyType === null){
                $data['error'] = \Config\Database\DBErrorName::$empty;
                return $data;
            }
            $data = array();
            try	{
                $stmt = $this->pdo->prepare('UPDATE `'.\Config\Database\DBConfig::$tableCar.'` SET
                `'.\Config\Database\DBConfig\Car::$brand.'`=:brand,`'
                .\Config\Database\DBConfig\Car::$model.'`=:model, `'
                .\Config\Database\DBConfig\Car::$yearProduction.'`=:yearProduction,`'
                .\Config\Database\DBConfig\Car::$engineSize.'`=:engineSize,`'
                .\Config\Database\DBConfig\Car::$enginePower.'`=:enginePower,`'
                .\Config\Database\DBConfig\Car::$registrationNumber.'`=:registrationNumber,`'
                .\Config\Database\DBConfig\Car::$vinNumber.'`=:vinNumber,`'
                .\Config\Database\DBConfig\Car::$fuel.'`=:fuel,`'
                .\Config\Database\DBConfig\Car::$colour.'`=:colour,`'
                .\Config\Database\DBConfig\Car::$idOwner.'`=:idOwner,`'
                .\Config\Database\DBConfig\Car::$idBodyType.'`=:idBodyType WHERE  `'
                .\Config\Database\DBConfig\Car::$idCar.'`=:idCar');  
 
                $stmt->bindValue(':idCar', $idCar, PDO::PARAM_INT);
                $stmt->bindValue(':brand', $brand, PDO::PARAM_STR);
                $stmt->bindValue(':model', $model, PDO::PARAM_STR);
                $stmt->bindValue(':yearProduction', $yearProduction, PDO::PARAM_STR);
                $stmt->bindValue(':engineSize', $engineSize, PDO::PARAM_STR);
                $stmt->bindValue(':enginePower', $enginePower, PDO::PARAM_INT);
                $stmt->bindValue(':registrationNumber', $registrationNumber, PDO::PARAM_STR);
                $stmt->bindValue(':vinNumber', $vinNumber, PDO::PARAM_STR);
                $stmt->bindValue(':fuel', $fuel, PDO::PARAM_STR);
                $stmt->bindValue(':colour', $colour, PDO::PARAM_STR);
                $stmt->bindValue(':idOwner', $idOwner, PDO::PARAM_INT);
                $stmt->bindValue(':idBodyType', $idBodyType, PDO::PARAM_INT);
                
                $result = $stmt->execute(); 
                $rows = $stmt->rowCount();;
                if(!$result)
                    $data['error'] = \Config\Database\DBErrorName::$nomatch;
                else
                    $data['message'] = \Config\Database\DBMessageName::$updateok;
                $stmt->closeCursor();                 
            }
            catch(\PDOException $e)	{
                $data['error'] = \Config\Database\DBErrorName::$query;
            }
            return $data;
		}         
    }        

