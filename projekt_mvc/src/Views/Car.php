<?php
	namespace Views;
	
	class Car extends View {
		public function getAll($data = null){        
            if(isset($data['message']))
                $this->set('message',$data['message']);
            if(isset($data['error']))
                $this->set('error',$data['error']);
			$model = $this->getModel('Car');
            $data = $model->getAll();
            $this->set('cars', $data['cars']);
            if(isset($data['error']))
                $this->set('error', $data['error']);
            $this->set('customScript', array('datatables.min', 'table.min'));
			$this->render('CarGetAll');
		}
		public function showone($idCar){
            $model = $this->getModel('Car');
            $data = $model->getOne($idCar);
            $this->set('cars', $data['cars']);
            if(isset($data['error']))
                $this->set('error', $data['error']);
            $this->render('CarGetOne');
		}
		public function getAllInBodyType($idBodyType) {
            $model = $this->getModel('Car');
            $data = $model->getBodyTypeinCar($idBodyType);  
            $this->set('cars', $data['cars']);
            if(isset($data['error']))
                $this->set('error', $data['error']);
            $this->set('customScript', array('datatables.min', 'table.min'));
			$this->render('CarGetAllInBodyType');            
        }
		public function getAllInOwner($idOwner) {
            $model = $this->getModel('Car');
            $data = $model->getOwnerInCar($idOwner); //-^ 
            $this->set('cars', $data['cars']);
            if(isset($data['error']))
                $this->set('error', $data['error']);
            $this->set('customScript', array('datatables.min', 'table.min'));
			$this->render('CarGetAllInOwner');            
        }
		
        public function addform(){
            $bodytypeModel = $this->getModel('BodyType');
            $bodytypes = $bodytypeModel->getAllForSelect();
            $this->set('bodytypes', $bodytypes); 
			$ownerModel = $this->getModel('Owner');
            $owners = $ownerModel->getAllForSelect();
            $this->set('owners', $owners);         			
            $this->set('customScript', array('jquery.validate.min', 'carform', 'CarAddForm'));
			$this->render('CarAddForm');
        }     
        public function editform($car){

			$bodytypeModel = $this->getModel('BodyType');
			$bodytypes = $bodytypeModel->getAllForSelect();
            $this->set('bodytypes', $bodytypes);
			$ownerModel = $this->getModel('Owner');
            $owners = $ownerModel->getAllForSelect();
            $this->set('owners', $owners);     			
            $this->set('idCar', $car[\Config\Database\DBConfig\Car::$idCar]);
            $this->set('brand', $car[\Config\Database\DBConfig\Car::$brand]);    
            $this->set('model', $car[\Config\Database\DBConfig\Car::$model]);
            $this->set('yearProduction', $car[\Config\Database\DBConfig\Car::$yearProduction]);
            $this->set('engineSize', $car[\Config\Database\DBConfig\Car::$engineSize]);
			$this->set('enginePower', $car[\Config\Database\DBConfig\Car::$enginePower]);
			$this->set('registrationNumber', $car[\Config\Database\DBConfig\Car::$registrationNumber]);
			$this->set('vinNumber', $car[\Config\Database\DBConfig\Car::$vinNumber]);
			$this->set('fuel', $car[\Config\Database\DBConfig\Car::$fuel]);
			$this->set('colour', $car[\Config\Database\DBConfig\Car::$colour]);
			$this->set('idOwner', $car[\Config\Database\DBConfig\Car::$idOwner]);
			$this->set('idBodyType', $car[\Config\Database\DBConfig\Car::$idBodyType]);
            
            $this->set('customScript', array('jquery.validate.min', 'carform', 'CarAddForm'));
			$this->render('CarEditForm');
        }        
        
	}



