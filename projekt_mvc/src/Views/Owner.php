<?php
	namespace Views;
	
	class Owner extends View {
		public function getAll($data = null){
            if(isset($data['message']))
                $this->set('message',$data['message']);
            if(isset($data['error']))
                $this->set('error',$data['error']); 
			$model = $this->getModel('Owner');
            $data = $model->getAll();
			$this->set('owners', $data['owners']);
            if(isset($data['error']))
                $this->set('error', $data['error']);
            $this->set('customScript', array('datatables.min', 'table.min'));
			$this->render('OwnerGetAll');
		}
        public function addform(){
            $this->set('customScript', array('jquery.validate.min', 'ownerform', 'OwnerAddForm'));
			$this->render('OwnerAddForm');
        }
        public function editform($Owner){
            $this->set('idOwner', $Owner[\Config\Database\DBConfig\Owner::$idOwner]);
            $this->set('name', $Owner[\Config\Database\DBConfig\Owner::$name]);
			$this->set('surname', $Owner[\Config\Database\DBConfig\Owner::$surname]);
			$this->set('phone', $Owner[\Config\Database\DBConfig\Owner::$phone]);
            $this->set('customScript', array('jquery.validate.min', 'ownerform', 'OwnerAddForm'));
			$this->render('OwnerEditForm');
        }        
	}


