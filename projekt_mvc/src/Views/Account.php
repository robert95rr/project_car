<?php
	namespace Views;
	
	class Account extends View {
		public function getAll($data = null){
            if(isset($data['message']))
                $this->set('message',$data['message']);
            if(isset($data['error']))
                $this->set('error',$data['error']); 
			$model = $this->getModel('Account');
            $data = $model->getAll();
			$this->set('accounts', $data['accounts']);
            if(isset($data['error']))
                $this->set('error', $data['error']);
            $this->set('customScript', array('datatables.min', 'table.min'));
			$this->render('AccountGetAll');
		}
        public function addform(){
            $this->set('customScript', array('jquery.validate.min', 'accountform', 'AccountAddForm'));
			$this->render('AccountAddForm');
        }
        public function editform($Account){
            $this->set('idAccount', $Account[\Config\Database\DBConfig\Account::$idAccount]);
            $this->set('login', $Account[\Config\Database\DBConfig\Account::$login]);
			$this->set('haslo', $Account[\Config\Database\DBConfig\Account::$haslo]);
            $this->set('customScript', array('jquery.validate.min','accountform', 'AccountAddForm'));
			$this->render('AccountEditForm');
        }        
	}


