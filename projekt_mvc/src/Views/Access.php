<?php
	namespace Views;

	class Access extends View {
		//wyświetla formularz do logowania
		public function logform($data = null) {
            if(isset($data['message']))
                $this->set('message',$data['message']);
            if(isset($data['error']))
                $this->set('error',$data['error']);

			$this->set('customScript', array('jquery.validate.min', 'logform'));
			$this->render('SignInForm');
		}

        //wyświetla formularz rejestracji użytownika
        public function signupform($data = null) {
            if(isset($data['message']))
                $this->set('message',$data['message']);
            if(isset($data['error']))
                $this->set('error',$data['error']);

			$this->set('customScript', array('jquery.validate.min', 'logform'));
			$this->render('CreateAccountForm');
		}
	}



