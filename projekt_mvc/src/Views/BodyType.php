<?php
	namespace Views;
	
	class BodyType extends View {
		public function getAll($data = null){
            if(isset($data['message']))
                $this->set('message',$data['message']);
            if(isset($data['error']))
                $this->set('error',$data['error']); 
			$model = $this->getModel('BodyType');
            $data = $model->getAll();
			$this->set('bodytypes', $data['bodytypes']);
            if(isset($data['error']))
                $this->set('error', $data['error']);
            $this->set('customScript', array('datatables.min', 'table.min', 'ajax'));
			$this->render('BodyTypeGetAll');
		}
        public function addform(){
            $this->set('customScript', array('jquery.validate.min', 'bodytypeform', 'BodyTypeAddForm'));
			$this->render('BodyTypeAddForm');
        }
        public function editform($BodyType){
            $this->set('idBodyType', $BodyType[\Config\Database\DBConfig\BodyType::$idBodyType]);
            $this->set('name', $BodyType[\Config\Database\DBConfig\BodyType::$name]);
            $this->set('customScript', array('jquery.validate.min', 'bodytypeform', 'BodyTypeAddForm'));
			$this->render('BodyTypeEditForm');
        }        
	}


