<?php
	namespace Config\Database;
	use \PDO; 
	
	class DBConnection{
		private static $databaseType;
		private static $hostname; 
		private static $port; 
		private static $user;
		private static $password;
		
		public static function setDBConnection(
            $user, $password, 
            $hostname, $databaseType, $port){
			DBConnection::$user = $user;
			DBConnection::$password = $password;
			DBConnection::$hostname = $hostname;
			DBConnection::$databaseType = $databaseType;
			DBConnection::$port = $port;
		}
		
		public static function getHandle(){
			try{
				$pdo = new PDO(
                    DBConnection::$databaseType.
                    ':hostname='.DBConnection::$hostname.
                    ';dbname='.DBConfig::$databaseName.
                    ';port='.DBConnection::$port, 
                    DBConnection::$user, DBConnection::$password );
				$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				return $pdo;				
			}catch(\PDOException $e){
                throw new \PDOException($e);
			}	
			return null;			
		}
	}
