<?php
	namespace Config\Database;

	class DBConfig{
        //nazwa bazy danych
        public static $databaseName = 'auto_projekt';
        //dane dostępowe do bazy danych
        public static $hostname = 'localhost';
        public static $databaseType = 'mysql';
        public static $port = '3306';
        public static $user = 'root';
        public static $password = '';
        //nazwy tabel
        public static $tableOwner = 'owner'; 
		public static $tableBodyType = 'bodytype';
		public static $tableCar = 'car';
		public static $tableAccount = 'account';
               
	}
