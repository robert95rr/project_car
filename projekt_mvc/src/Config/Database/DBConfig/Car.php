<?php
	namespace Config\Database\DBConfig;

    class Car {
        public static $idCar = 'idCar';
		public static $brand = 'brand';
		public static $model = 'model';
		public static $yearProduction = 'yearProduction';
		public static $engineSize = 'engineSize';
		public static $enginePower = 'enginePower';
		public static $registrationNumber = 'registrationNumber';
		public static $vinNumber = 'vinNumber';
		public static $fuel = 'fuel';
		public static $colour = 'colour';
		public static $idOwner = 'idOwner';
		public static $idBodyType = 'idBodyType';
        
    }