<?php
	namespace Config\Database\DBConfig;

    class Owner {
        public static $idOwner = 'idOwner';
        public static $name = 'name';
        public static $surname = 'surname';
        public static $phone = 'phone';
    }
