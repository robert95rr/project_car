<?php
	namespace Config\Website;
		
	class MessageName{
		public static $mustlogin = "Aby wykonać operację musisz się zalogować!";    
		public static $logout = "Jesteś wylogowany!";
		public static $login = "Jesteś zalogowany!";
		public static $signup = "Jesteś zarejestrowany. Możesz się teraz zalogować!";
	}
