<?php
	namespace Tools;

	class Access extends Session {
		private static $login = 'user';
        private static $userid = 'userid';
		private static $loginTime = 'logintime';
		private static $sessionTime = 60*20;

		//zaloguj
		public static function login($login, $idAccount) {//ZMIANA
			//sprawdzenie istniejącej sesji
			if(parent::check() === true)
			{
				//zmieniając poziom dostępu regenrerujemy sesję
				parent::regenerate();
				parent::set(self::$login, $login);
                parent::set(self::$userid, $idAccount);//Zmiana!!!!!!!!!!!!!!
				parent::set(self::$loginTime, time());
			}
		}
		//wyloguj
		public static function logout() {
			parent::clear(self::$login);
			parent::clear(self::$loginTime);
			parent::regenerate();
		}
		//sprawdź czy jest zalogowany
		public static function islogin() {
			if(parent::is(self::$login) === true) {

				if(time() > parent::get(self::$loginTime) + self::$sessionTime) {
					//przekroczono czas sesji, wyloguj
					self::logout();
					return false;
				}
				parent::set(self::$loginTime, time());
				return true;
			}
			return false;
		}

        public static function getuserid() {
            return parent::get(self::$userid);
        }
	}
